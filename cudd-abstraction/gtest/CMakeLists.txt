
if ("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
    add_definitions(-DGTEST_HAS_TR1_TUPLE=0)
endif ()

add_library(gtest
  gtest-all.cc gtest.h)

