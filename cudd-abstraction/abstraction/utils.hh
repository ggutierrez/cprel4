#ifndef __UTILS__HH__
#define __UTILS__HH__

#include "format.h"
#include <string>
#include <sstream>
#include <fstream>
#include <chrono>
#include <ctime>
/**
 * Utility functions to print output.
 */
template <typename... Args>
void printError(const Args&... args) {
  fmt::print_colored(fmt::RED, args...);
}

template <typename... Args>
void printInfo(const Args&... args) {
  fmt::print_colored(fmt::BLUE, args...);
}

template <typename... Args>
void printDebug(const Args&... args) {
  fmt::print_colored(fmt::GREEN, args...);
}

#endif