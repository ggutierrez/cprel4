#ifndef __AGG_RELATION_DECL_HH__
#define __AGG_RELATION_DECL_HH__

namespace CuddAbstraction {

// Forward declaration of relation
class Relation;

class AggRelation {
private:
  // Schema of the relation
  Schema schema_;
  // Relation data
  ADD data_;

protected:
  AggRelation(ADD f, const Schema& sch);

public:
  /// @brief Constructors
  ///@{
  // Default constructor
  AggRelation(void);
  /**
   * @brief Constructs an aggregated relation on the provided schema @a sch
   */
  AggRelation(const SpaceManager& home, const Schema& sch);
  /**
   * @brief Constructs an aggregated relation with the schema defined by the
   *     array of attributes @a sch.
   */
  AggRelation(const SpaceManager& home, const std::vector<Attribute>& sch);
  /// Copy constructor
  AggRelation(const AggRelation& r);
  /**
   *  @brief  Assignment operator
   */
  AggRelation& operator=(const AggRelation& right);
  ///@}

  /// @brief Relation information
  ///@{
  /**
   * @brief Returns the schema of this relation
   */
  const Schema& schema(void) const { return schema_; }
  /**
   * @brief Returns the number of attributes
   */
  size_t arity(void) const { return schema_.arity(); }
  /**
   * @brief Returns the cardinality of the relation
   */
  double cardinality(void) const;
  ///@}

  /// @brief Modifiers
  ///@{
  /**
   * @brief Adds the tuple defined by @a args to the aggregated relation with an
   *     associated value @a agg.
   * - @a args can only consists of integer numbers and must be valid in the
   *   domain of every attribute.
   * - The i-th element in @a args corresponds to the i-th attribute in the
   *   declaration of the schema.
   */
  template <typename... Args>
  void add(const SpaceManager& home, int agg, Args... args);
  /**
   * @brief Adds the tuple @a c to this relation with aggregated value @a agg
   *
   * The elements in @a t are assumed to match, in its order, to the attributes
   * in the schema of this relation.
   */
  void add(const SpaceManager& home, int agg, const NumericTuple& t);

  ///@}
  /// @brief Transformations
  ///@{
  /**
   * @brief Returns an aggregated relation from  relation value @a r
   *
   * Every tuple that belongs to @a r is associated with an aggregated value of
   * 1 and any other tuple to 0.
   */
  static AggRelation fromRelation(const Relation& r);
  /**
   * @brief Creates an aggregated relation as the result of grouping relation @a
   *     r by attributes @a groupedAttributes.
   *
   * The operation applied among the elements of each group is group cardinality
   * and its result gets associated with each tuple in the aggregated relation.
   */
  static AggRelation groupBy(const SpaceManager& home, const Relation& r,
                             const std::vector<Attribute>& groupedAttributes);
  /**
   * @brief Returns a relation with the tuples that have an aggregated value in
   *     the closed interval formed by  @a lb and @a ub.
   */
  Relation filterInterval(const SpaceManager& home, int lb, int ub) const;
  /**
   * @brief Returns a relation with the tuples that have an aggregated value
   *     equal to @a a
   */
  Relation filterValue(const SpaceManager& home, int a) const;
  /**
   * @brief Returns a relation by considering every discriminant of the
   *     aggregation as the value of attribute @a a.
   *
   * @warn It is the callee responsibility to ensure that all the aggregated
   *     values belong to the domain of attribute @a a.
   */
  Relation aggregatedAsAttribute(const SpaceManager& home,
                                 const Attribute& a) const;
  ///@}
  ///// @brief Access
  ///@{
  /**
   * @brief Returns the minimum value in the aggregation
   */
  int min(const SpaceManager& home) const;
  /**
   * @brief Returns the maximum value in the aggregation
   */
  int max(const SpaceManager& home) const;
  /**
   * @brief Returns the minimum non zero ranking of the relation
   *
   * If all the tuples in the relation are ranked with zero then 0 is returned.q
   */
  int minNonzero(const SpaceManager& home) const;
  ///@}
  /// @brief Operations
  ///@{
  /**
   * @brief Computes the join between this aggregated relation and relation @a r
   *
   * Notice that @a r must be a relation and not an aggregated relation since
   * boolean algebra is not defined on any integers. The operation takes into
   * account the tuples in @a r and "looks" for matches with the tuples in this
   * relation. Those tuples that match are in the result with its aggregated
   * value  in this relation.
   */
  AggRelation joinWith(const SpaceManager& home, const Relation& r) const;
  ///@}
  ///// @brief Output
  ///@{
  /**
   * @brief Outputs a dot representation of the relation aggregation
   */
  void toDot(const SpaceManager& home, const char* fileName,
             const char* funcName) const;
  ///@}
};
}
#endif