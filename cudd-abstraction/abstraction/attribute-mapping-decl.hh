#ifndef __ATTRIBUTE_MAPPING_DECL_HH
#define __ATTRIBUTE_MAPPING_DECL_HH

#include "exception-decl.hh"
#include "schema-decl.hh"
#include "set-space-decl.hh"

#include "cuddObj.hh"
#include <vector>

// The following code tell clang to behave silent with the specific warning.
// This is a problem of boost as of today:
// https://svn.boost.org/trac/boost/ticket/8743
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wredeclared-class-member"
#include <boost/bimap.hpp>
#pragma clang diagnostic pop

namespace CuddAbstraction {

namespace bb = boost::bimaps;

class AttributeMap {
private:
  // Definitions for the bimap container
  /// Left hand side tag
  struct lhs {};
  /// Right hand side tag
  struct rhs {};
  /// Type definition for the storage for the attribute map
  typedef bb::bimap<bb::tagged<Attribute, lhs>, bb::tagged<Attribute, rhs>>
      Mapping;
  /// Type definition for entries in Mapping
  typedef Mapping::value_type MapEntry;

private:
  /// Storage for the mapping
  Mapping attributeMap_;

public:
  /// Creates an empty map
  AttributeMap(void);
  /**
   * @brief Creates an attribute mapping as specified by every pair in @a m
   *
   */
  AttributeMap(const std::initializer_list<std::pair<Attribute, Attribute>>& m);
  /// Copy constructor
  AttributeMap(const AttributeMap& m);
  /// Destructor
  ~AttributeMap(void);

public:
  // TODO: the input arguments of the following method should be references.
  /// Adds the entry <@a a0, @a a1> to the map
  void add(const Attribute& a0, const Attribute a1);

public:
  /// Tests if attribute @a a is in the LHS of this map
  bool isInLHS(const Attribute& a) const;
  /// Tests if attribute @a a is in the RHS of this map
  bool isInRHS(const Attribute& a) const;
  /// Returns the attribute in the RHS that is mapped to @a a
  const Attribute& getRHSOf(const Attribute& a) const;
  /// Returns the attribute in the LHS that is mapped to @a a
  const Attribute& getLHSOf(const Attribute& a) const;
  /// Returns the number of entries in the map
  size_t size(void) const;
  /// Apply function @a fun on every entry in the map
  void
  apply(std::function<void(const Attribute& l, const Attribute& r)> fun) const;

public:
  /// Returns the inverse of this map
  AttributeMap inverse(void) const;

public:
  /// Tests whether the mapping is an identity
  bool isIdentity(void) const;
  /// Tests whether this map has a mapping for every attribute of schema @a s
  bool mapsAllAttributesIn(const Schema& s) const;
  /**
   * @brief Tests whether this mapping maps all the attributes of @a s0 to
   *     attributes of @a s1
   */
  bool mappingBetween(const Schema& s0, const Schema& s1) const;

public:
  /**
   * @brief Returns the correspondence represented by the map as a
   *     correspondence between arrays of BDD variables.
   *
   * The first element of the pair contains BDD variables of the LHS and the
   * second the ones in the RHS. The correspondence between the BDD variables is
   * implicitly indicated by the position of the variables in both vectors. This
   * operation throws an exception if at least one of the entries in the map
   * does not have the same representation size for the LHS attribute and the
   * RHS attribute.
   */
  std::pair<std::vector<BDD>, std::vector<BDD>>
  BDDMapping(const SpaceManager& home) const;
};
/**
 * @brief Output of a map of attributes
 */
std::ostream& operator<<(std::ostream& os, const AttributeMap& m);
/**
 * @brief Exception thrown by operations that require a valid domain mapping
 */
class InvalidAttributeMap : public Exception {
public:
  InvalidAttributeMap(void) = delete;
  InvalidAttributeMap(const char* location)
      : Exception(location, "Invalid attribute map") {}
};

/**
 * @brief Exception thrown when trying to map two attributes that have a
 *     different domain.
 */
class InvalidAttributeDomainMapping : public Exception {
public:
  InvalidAttributeDomainMapping(void) = delete;
  InvalidAttributeDomainMapping(const char* location)
      : Exception(location,
                  "Attempt to map two attributes from different domains") {}
};
}

#endif