/**
 * This file is not being used. It contains the C++ implementations of unique
 * abstractions but after testing them the performance is not worth it. The
 * file is kept for reference anyhow.
 */

#include <cuddObj.hh>

namespace CuddAbstraction {

inline int level(const Cudd& home, BDD d) {
  return home.ReadPerm(d.NodeReadIndex());
}

inline BDD regular(const Cudd& home, BDD r) {
  return BDD(home, r.getRegularNode());
}

inline BDD high(const Cudd& home, const BDD& r) {
  DdNode* d = r.getNode();
  DdNode* res = Cudd_T(d);
  res = Cudd_NotCond(res, Cudd_IsComplement(d));
  return BDD(home, res);
}

inline BDD low(const Cudd& home, const BDD& r) {
  DdNode* d = r.getNode();
  DdNode* res = Cudd_E(d);
  res = Cudd_NotCond(res, Cudd_IsComplement(d));
  return BDD(home, res);
}

inline BDD topVar(const Cudd& home, BDD r) {
  return home.ReadVars(r.NodeReadIndex());
}

BDD uniqueAbstractImplSE(const Cudd& home, BDD f, BDD cube) {
  BDD one = home.bddOne();
  BDD zero = home.bddZero();

  if (cube.IsOne())
    return f;
  if (f.IsZero() || f.IsOne())
    return zero;

  int topf = level(home, f);
  int topc = level(home, cube);

  BDD F = regular(home, f);

  BDD T = high(home, F);
  BDD E = low(home, F);
  if (f != F) {
    T = !T;
    E = !E;
  }

  if (topf < topc) { // topvar of f is above top var of c. Hence, topvar of f is
                     // not abstracted.
    BDD uaElse = uniqueAbstractImplSE(home, E, cube);
    BDD uaThen = uniqueAbstractImplSE(home, T, cube);
    return topVar(home, F).Ite(uaThen, uaElse);
  } else if (topc < topf) { // topvar of c is above top var of f. Hence, topc is
                            // a don't care in f.
    return zero;
  } else { // variables at the top of f and the cube are the same. In this case
           // the variable has to be abstracted.
    cube = high(home, cube);
    if (E.IsZero())
      return uniqueAbstractImplSE(home, T, cube);
    else if (T.IsZero())
      return uniqueAbstractImplSE(home, E, cube);
    else {
      BDD EX = E.ExistAbstract(cube);
      BDD TX = T.ExistAbstract(cube);
      if (EX == TX)
        return zero;

      BDD iteElseT = EX.Ite(zero, T);
      BDD iteThenE = TX.Ite(zero, E);

      BDD R = iteElseT.Ite(one, iteThenE);
      return uniqueAbstractImplSE(home, R, cube);
    }
  }
}

BDD uniqueAbstractImpl(const Cudd& home, BDD f, BDD cube) {
  BDD one = home.bddOne();
  BDD zero = home.bddZero();

  if (cube.IsOne())
    return f;

  if ((f.IsZero() || f.IsOne()) && !cube.IsOne())
    return zero;

  int topf = level(home, f);
  int topc = level(home, cube);

  if (topf > topc)
    return zero;

  BDD F = regular(home, f);

  BDD T = high(home, F);
  BDD E = low(home, F);
  if (f != F) {
    T = !T;
    E = !E;
  }

  if (topf == topc) {
    BDD uniqueThen = uniqueAbstractImpl(home, T, high(home, cube));
    BDD existElse = E.ExistAbstract(high(home, cube));
    BDD resultThen = uniqueThen & !existElse;

    BDD existThen = T.ExistAbstract(high(home, cube));
    BDD uniqueElse = uniqueAbstractImpl(home, E, high(home, cube));
    BDD resultElse = uniqueElse & !existThen;

    return resultThen | resultElse;
  } else {
    BDD uniqueThen = uniqueAbstractImpl(home, T, cube);
    BDD uniqueElse = uniqueAbstractImpl(home, E, cube);
    return topVar(home, F).Ite(uniqueThen, uniqueElse);
  }
}
}
