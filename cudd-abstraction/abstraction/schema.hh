#ifndef __SCHEMA_HH__
#define __SCHEMA_HH__

#include "schema-decl.hh"
#include "format.h"

#include <cassert>
#include <string>
#include <sstream>
#include <utility>
#include <numeric>

namespace CuddAbstraction {

//////////////////
// Constructors //
//////////////////

inline Schema::Schema(void)
    : domain_(0) {
  // TODO: attribute cube_ should be initialized with the true BDD. However, so
  // far I have not an static method in SpaceManager to get it and this is the
  // default constructor so we cannot take arguments.
}

inline Schema::Schema(const SpaceManager& home) {
  // domain_ is default initialized to the empty vector of attributes
  cube_ = home.bddOne();
}

inline Schema& Schema::operator=(const Schema& right) {
  if (this != &right) {
    domain_ = right.domain_;
    cube_ = right.cube_;
  }
  return *this;
}

inline Schema::Schema(const SpaceManager& home, const std::vector<Attribute>& d)
    : domain_(d) {
  LOG_OPERATION_NARGS("Sch::attrCtor");
  cube_ = home.bddOne();
  for (const auto& d : domain_)
    cube_ &= d.asCube();
  assert(cube_.IsCube());
}

inline Schema::Schema(const Schema& d)
    : domain_(d.domain_)
    , cube_(d.cube_) {
  LOG_OPERATION_NARGS("Sch::cctor");
}

////////////////////////
// Schema information //
////////////////////////
inline size_t Schema::reprSize(void) const {
  LOG_OPERATION_NARGS("Sch::reprSize");
  return std::accumulate(domain_.begin(), domain_.end(), 0,
                         [](size_t acc, const Attribute& a) -> size_t {
                           return acc + a.reprSize();
                         });
}

/////////////////////////
// Schema modification //
/////////////////////////
inline void Schema::add(const Attribute& vdom) {
  LOG_OPERATION_NARGS("Sch::addAttr");
  // TO_OPTIMIZE: It should be faster to test vdom.asCube() <= cube_ or we can
  //   also make this test based on the attributes in the vector
  if (SpaceManager::variablesSubset(vdom.asCube(), cube_)) {
    // nothing to do as vdom is already a member of this domain
    return;
  }
  if (isEmpty()) {
    domain_.push_back(vdom);
    cube_ = vdom.asCube();
    return;
  }
  // not part and this is not empty
  domain_.push_back(vdom);
  cube_ &= vdom.asCube();
}

/////////////////////////
// Encoding / Decoding //
/////////////////////////
inline BDD Schema::asBDD(const SpaceManager& home,
                         const NumericTuple& d) const {
  LOG_OPERATION_NARGS("Sch::tupleAsBDD");
  assert(d.size() == arity());

  BDD result = home.bddOne();
  for (size_t i = 0; i < arity(); i++) {
    AttributeDomain ad = domain_[i].domain();
    if (!ad->valid(d[i]))
      throw InsufficientDomainSize("Schema::asBDD");
    BDD encI = domain_[i].asBDD(home, d[i]);
    result &= encI;
  }
  return result;
}

inline NumericTuple Schema::decodeAssignment(int* a) const {
  LOG_OPERATION_NARGS("Sch::bddAsTuple");
  NumericTuple t;
  t.reserve(domain_.size());
  for (const auto& d : domain_) {
    std::vector<int> minterm = d.select(a);
    IntegerDatum v = d.asInteger(minterm.data());
    t.push_back(std::move(v));
  }
  return t;
}

inline std::vector<NumericTuple> Schema::asTuples(const SpaceManager& home,
                                                  BDD f) const {
  LOG_OPERATION_NARGS("Sch::asTuples");
  std::vector<NumericTuple> result;
  // prepare a collection of the BDD variables' indexes that concern to this
  // domain
  std::set<unsigned int> indices;
  for (const auto& d : domain_) {
    const auto& i = d.getIndices();
    indices.insert(std::begin(i), std::end(i));
  }
  // - Visit all the cubes of f.
  cubeVisitor(f, [&home, &indices, &result, this](int* cube) -> void {
    // - A cube of f is an array of variables that an be indexed by bdd
    //   variables index. It will have either 0, 1 or 2 as values, we have to
    //   be careful because the size of the array is the number of bdd
    //   variables in the _manager_. We need to filter only the variables that
    //   correspond to f.
    // - Moreover, we use an assignment visitor on the cube that generates all
    //   possible states for the variables that concern to f.
    // - indices is internally used by the assignment visitor to take into
    //   account indices that only concern to f.
    assignementVisitor(cube, home.ReadSize(), indices,
                       [&result, this](int* assign) -> void {
                         result.push_back(decodeAssignment(assign));
                       });
  });
  return result;
}

inline Attribute Schema::attribute(const std::string& name) const {
  LOG_OPERATION_NARGS("Sch::attr");
  for (const Attribute& att : domain_)
    if (att.name() == name)
      return att;
  // TODO: This should throw an exception or something reliable in release mode
  assert(false);
  return Attribute();
}

///////////
// Tests //
///////////
inline bool Schema::sameAs(const Schema& d) const {
  LOG_OPERATION_NARGS("Sch::equal", d);
  return cube_ == d.cube_;
}

inline bool Schema::overlapsWith(const Schema& d) const {
  LOG_OPERATION_NARGS("Sch::overlaps", *this);
  // TO_OPTIMIZE: Could I use the <= operator on BDD to make this faster?
  return SpaceManager::haveVarsInCommon(cube_, d.cube_);
}

inline bool Schema::disjointWith(const Schema& d) const {
  LOG_OPERATION_NARGS("Sch::disj", *this);
  return !overlapsWith(d);
}

inline bool Schema::contains(const Attribute& d) const {
  LOG_OPERATION_NARGS("Sch::member", *this);
  // TO_OPTIMIZE: Could I use the <= operator on BDD to make this faster?
  return SpaceManager::variablesSubset(d.asCube(), cube_);
}

inline bool Schema::contains(const Schema& d) const {
  // TO_OPTIMIZE: It should be simpler to see if cube <= d.cube
  LOG_OPERATION_NARGS("Sch::subset", *this);
  return SpaceManager::variablesSubset(d.asCube(), cube_);
}

inline Schema Schema::unionWith(const SpaceManager& home,
                                const Schema& d) const {
  LOG_OPERATION_NARGS("Sch::union", *this);
  Schema result(*this);
  for (const Attribute& i : d.domain_)
    result.add(i);
  return result;
}

inline Schema Schema::differenceWith(const SpaceManager& home,
                                     const Schema& d) const {
  LOG_OPERATION_NARGS("Sch::diff", *this);
  Schema result(home);
  for (const Attribute& i : domain_)
    if (!d.contains(i))
      result.add(i);
  return result;
}

inline Schema Schema::intersectWith(const SpaceManager& home,
                                    const Schema& d) const {
  LOG_OPERATION_NARGS("Sch::intersect");
  Schema result(home);
  for (const Attribute& i : d.domain_)
    if (contains(i))
      result.add(i);
  return result;
}

inline Schema Schema::symDifferenceWith(const SpaceManager& home,
                                        const Schema& d) const {
  LOG_OPERATION_NARGS("Sch::symDiff");
  Schema thisMinusD = differenceWith(home, d);
  Schema dMinusThis = d.differenceWith(home, *this);
  return thisMinusD.unionWith(home, dMinusThis);
}

/////////////////
// Dot support //
/////////////////
inline SpaceManager::LevelNamesMapType
Schema::levelNames(const SpaceManager& home) const {
  SpaceManager::LevelNamesMapType names = home.extractLevels(cube_.getNode());
  for (const auto& att : domain_) {
    const std::string& name = att.name();
    DdNode* cube = att.asCube().getNode();
    int i = 0;
    visitBDDNodes(cube, [&](DdNode* n) {
      int levelOfN = home.level(n);
      names[levelOfN] = name + "_" + std::to_string(i);
      i++;
    });
  }
  return names;
}

inline void Schema::toDot(const SpaceManager& home, const char* fileName,
                          const char* funcName, bool asADD) const {

  DdNode* cube = asADD ? cube_.Add().getNode() : cube_.getNode();
  home.toDot(fileName, cube, funcName, levelNames(home));
}

inline std::string Schema::debugInfo(bool varInfo) const {
  fmt::MemoryWriter out;
  out.write("{{");
  out.write("\"repr size\": {},", reprSize());
  out.write("\"arity\": {},", arity());
  out.write("\"domain\": [");
  for (size_t i = 0; i < domain_.size(); i++) {
    out.write("{}\n", domain_[i].debugInfo(varInfo));
    if (i < (domain_.size() - 1))
      out.write(",");
  }
  out.write("]");
  out.write("}}");
  return out.str();
}

inline bool setCompatible(const Schema& r, const Schema& s, const Schema& t) {
  LOG_OPERATION_NARGS("Sch::setCompat3");
  return (r == s) && (s == t);
}

inline bool setCompatible(const Schema& r, const Schema& s) {
  LOG_OPERATION_NARGS("Sch::setCompat");
  return r == s;
}

inline bool joinCompatible(const SpaceManager& home, const Schema& r,
                           const Schema& s, const Schema& t) {
  LOG_OPERATION_NARGS("Sch::joinCompat", r, s, t);
  return t == (r.unionWith(home, s));
}

inline bool composeCompatible(const SpaceManager& home, const Schema& r,
                              const Schema& s, const Schema& t) {
  LOG_OPERATION_NARGS("Sch::composeCompat", r, s, t);
  // Attributes on which composition is performed
  Schema compose = r.intersectWith(home, s);
  return t == r.unionWith(home, s).differenceWith(home, compose);
}

inline bool projectCompatible(const Schema& r, const Schema& p,
                              const Schema& t) {
  LOG_OPERATION_NARGS("Sch::projectCompat", r, p, t);
  return r.contains(p) && (t == p);
}

inline bool operator==(const Schema& d, const Schema& e) { return d.sameAs(e); }

inline bool operator!=(const Schema& d, const Schema& e) { return !(d == e); }

inline std::ostream& operator<<(std::ostream& os, const Schema& d) {
  LOG_OPERATION_NARGS("Sch::output");
  os << "<";
  for (const Attribute& i : d.asColumnDomains())
    os << i << " ";
  os << ">\n";
  return os;
}
}

#endif