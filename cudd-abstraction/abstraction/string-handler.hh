#ifndef __STRING_HANDLER_HH__
#define __STRING_HANDLER_HH__

#include "string-handler-decl.hh"

namespace CuddAbstraction {
/////////////////
// DomainGroup //
/////////////////
inline DomainGroup::DomainGroup(void) {}

inline void DomainGroup::add(AttributeDomain d) {
  map_.emplace(std::make_pair(d, DomainMapping()));
  widths_[d] = 0;
}

inline void DomainGroup::add(const Attribute& a) { add(a.domain()); }

inline DomainGroup::DomainMapping&
DomainGroup::getMapping(const Attribute& att) {
  AttributeDomain dom = att.domain();
  auto it = map_.find(dom);
  assert(it != map_.end() && "Attribute is not part of the group");
  return it->second;
}

inline bool DomainGroup::isThereMappingFor(const Attribute& att) const {
  AttributeDomain dom = att.domain();
  auto it = map_.find(dom);
  return it != map_.end();
}

inline IntegerDatum DomainGroup::getEncoding(const std::string& str,
                                             const Attribute& att) {
  DomainMapping& attMapping = getMapping(att);
  // First possibility: there is already an encoding for str and we just return
  // it.
  auto it = attMapping.by<value>().find(str);
  if (it != attMapping.by<value>().end()) {
    return it->second;
  }
  // Second possibility: the encoding does not exist but we can create one
  if (attMapping.size() < att.domain()->maxCardinality()) {
    IntegerDatum enc(attMapping.size());
    attMapping.insert(Entry(str, enc));
    widths_[att.domain()] = std::max(widths_[att.domain()], str.length());
    return enc;
  }
  // All the elements of the attribute's domain are already used
  throw InsufficientDomainCardinality("DomainGroup::getEncoding");
  return IntegerDatum();
}

inline const std::string&
DomainGroup::getValueEncodedBy(const IntegerDatum& enc, const Attribute& att) {
  DomainMapping& attMapping = getMapping(att);
  auto it = attMapping.by<encoding>().find(enc);
  if (it != attMapping.by<encoding>().end()) {
    return it->second;
  }
  throw InvalidEncoding("getEncodedBy");
}
inline bool DomainGroup::hasEntriesFor(const Schema& sch) const {
  const auto& attributes = sch.asColumnDomains();
  return std::any_of(
      attributes.cbegin(), attributes.cend(),
      [&](const Attribute& a) -> bool { return isThereMappingFor(a); });
}

inline size_t DomainGroup::getWidth(const Attribute& att) const {
  auto it = widths_.find(att.domain());
  assert(it != widths_.end() && "Attribute domain not found in the group");
  return it->second;
}

inline DomainGroup& operator<<(DomainGroup& dg, const Attribute& a) {
  dg.add(a);
  return dg;
}

namespace internal {
inline void getEncodings(DomainGroup& dg, const Schema& sch, int i,
                         NumericTuple& et) {}

template <typename... Args>
void getEncodings(DomainGroup& dg, const Schema& sch, int i, NumericTuple& et,
                  const std::string& val, Args... args) {
  const Attribute& attI = sch.asColumnDomains().at(i);
  et[i] = dg.getEncoding(val, attI);
  getEncodings(dg, sch, i + 1, et, args...);
}
}

template <typename... Args>
void insert(const SpaceManager& home, DomainGroup& dg, Relation& rel,
            Args... args) {
  if (sizeof...(Args) != rel.schema().arity()) {
    assert(false && "Number of elements does not match relation arity");
    // TODO: Throw an exception instead
  }
  assert(dg.hasEntriesFor(rel.schema()) &&
         "Relation not handled by domain group");
  NumericTuple et(rel.arity());
  internal::getEncodings(dg, rel.schema(), 0, et, args...);
  rel.add(home, et);
}

template <typename It>
void insertIT(const SpaceManager& home, DomainGroup& dg, Relation& rel,
              It start, It end) {
  size_t i = 0;
  NumericTuple et(rel.arity());
  assert(dg.hasEntriesFor(rel.schema()) &&
         "Relation not handled by domain group");
  while (i < rel.arity() && start != end) {
    const Attribute& att = rel.schema().asColumnDomains()[i];
    et[i] = dg.getEncoding(*start, att);
    i++;
    ++start;
  }
  assert((i == rel.arity() || start != end) &&
         "The iterator describes a number of elements "
         "different from the arity of the relation");
  if (i != rel.arity() || start != end) {
    // TODO: throw an exception
    return;
  }
  rel.add(home, et);
}

inline void readFromStream(const SpaceManager& home, DomainGroup& dg,
                           Relation& rel, std::istream& input) {
  std::string line;
  while (std::getline(input, line)) {
    std::stringstream is(line);
    insertIT(home, dg, rel, std::istream_iterator<std::string>(is),
             std::istream_iterator<std::string>());
  }
}

template <typename... Args>
void insert(const SpaceManager& home, DomainGroup& dg, AggRelation& rel,
            int agg, Args... args) {
  if (sizeof...(Args) != rel.schema().arity()) {
    assert(false && "Number of elements does not match number of attributes");
    // TODO: Throw an exception instead
  }
  assert(dg.hasEntriesFor(rel.schema()) &&
         "Relation not handled by domain group");
  NumericTuple et(rel.arity());
  internal::getEncodings(dg, rel.schema(), 0, et, args...);
  rel.add(home, agg, et);
}
}

#endif