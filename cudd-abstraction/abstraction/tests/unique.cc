#include <gtest/gtest.h>

#include "bdd-abstraction.hh"

#include <random>
#include <vector>

using namespace std;
using namespace CuddAbstraction;

TEST(UniqueAbs, abstractEmpty) {
  // Any unique abstraction on a relation that is empty should give the empty
  // relation as result.
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(2)), c1(home, makeDomain(2));

    Relation emptyRelation(home, {c0, c1});

    Relation absFromEmtpy = emptyRelation.uniqueAbstract(home, {c0});
    EXPECT_TRUE(absFromEmtpy.isEmpty());

    Relation absFromEmpty2 = emptyRelation.uniqueAbstract(home, {c1});
    EXPECT_TRUE(absFromEmpty2.isEmpty());
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(UniqueAbs, dontCares) {
  // Abstracting a domain that is represented as a _don't care_ in the BDD of
  // the relation should work as expected.
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(2)), c1(home, makeDomain(2));
    Relation c(home, {c0, c1});

    c.add(home, {1, 0});
    c.add(home, {1, 1});

    // With the tuples above the BDD representing the relation does not contain
    // a node to represent c1. That is, c1 is a don't care.
    // Whatever the representation, after abstracting c1 uniquely we should get
    // an empty relation because both tuples contain 1 in c0.
    Relation r = c.uniqueAbstract(home, {c1});

    Relation empty(home, {c0});
    EXPECT_TRUE(r.isEmpty());
    EXPECT_TRUE(r.sameAs(empty));
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(UniqueAbs, dontCaresB) {
  // Abstracting a domain that is represented as a _don't care_ in the BDD of
  // the relation should work as expected.
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(2)), c1(home, makeDomain(2));
    Relation c(home, {c0, c1});

    c.add(home, {0, 1});
    c.add(home, {1, 1});

    // With the tuples above the BDD representing the relation does not contain
    // a node to represent c0. That is, c0 is a don't care.
    // Whatever the representation, after abstracting c0 uniquely we should get
    // an empty relation because both tuples contain 1 in c1.
    Relation r = c.uniqueAbstract(home, {c0});

    Relation empty(home, {c1});
    EXPECT_TRUE(r.isEmpty());
    EXPECT_TRUE(r.sameAs(empty));
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(UniqueAbs, dontCaresC) {
  // Abstracting a domain that is represented as a _don't care_ in the BDD of
  // the relation should work as expected.
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(2));
    Relation c(home, {c0});

    c.add(home, {0});
    c.add(home, {1});

    // Abstracting the only possible column in a relation should give as result
    // the empty relation. The reason behind this is the intrinsic nature of
    // abstraction which is about the removal of columns.
    Relation r = c.uniqueAbstract(home, {c0});

    EXPECT_TRUE(r.isEmpty());
    EXPECT_TRUE(r.schema().isEmpty());
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(UniqueAbs, cubeAbstraction) {
  // Test that the variables mentioned in a cube are actually abstracted by
  // unique quantification
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(2)), c1(home, makeDomain(2)),
        c2(home, makeDomain(2)), c3(home, makeDomain(2));
    Relation c(home, {c0, c1, c2, c3});

    c.add(home, {0, 0, 0, 0});
    c.add(home, {0, 0, 1, 0});

    // Abstracting c3 results in a relation with the same two tuples. The reason
    // is that everything remains unique after removing c3.
    Relation r = c.uniqueAbstract(home, {c3});

    Relation expected(home, {c0, c1, c2});
    expected.add(home, {0, 0, 0});
    expected.add(home, {0, 0, 1});
    EXPECT_TRUE(r.sameAs(expected));
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(UniqueAbs, cubeAbstractionB) {
  // Test that the variables mentioned in a cube are actually abstracted by
  // unique quantification
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(2)), c1(home, makeDomain(2)),
        c2(home, makeDomain(2)), c3(home, makeDomain(2));
    Relation c(home, {c0, c1, c2, c3});

    c.add(home, {0, 0, 1, 0}); // <0,1>
    c.add(home, {0, 0, 1, 1}); // <0,3>

    // Abstracting c2 and c3 results in an empty relation: after removing them,
    // the tuples in the relation become equal and hence non unique.
    Relation r = c.uniqueAbstract(home, {c2, c3});

    Relation expected(home, {c0, c1});
    EXPECT_TRUE(r.sameAs(expected));

    // Abstracting c0 and c1 will give us the same relation because the
    // remainings of the tuples are unique.
    Relation r2 = c.uniqueAbstract(home, {c0, c1});

    Relation expected2(home, {c2, c3});
    expected2.add(home, {1, 0});
    expected2.add(home, {1, 1});

    EXPECT_TRUE(r2.sameAs(expected2));
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(UniqueAbs, cubeAbstractionC) {
  // Test that the variables mentioned in a cube are actually abstracted by
  // unique quantification
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(2)), c1(home, makeDomain(2)),
        c2(home, makeDomain(2)), c3(home, makeDomain(2));

    Relation c(home, {c0, c1, c2, c3});

    c.add(home, {0, 1, 0, 0}); // <2,0>
    c.add(home, {0, 1, 1, 0}); // <2,1>
    c.add(home, {1, 1, 0, 0}); // <3,0>
    c.add(home, {1, 1, 1, 0}); // <3,1>

    // Abstracting c0 and c1 results in the empty relation
    Relation r = c.uniqueAbstract(home, {c0, c1});

    Relation expected(home, {c2, c3});
    EXPECT_TRUE(r.sameAs(expected));

    // Abstracting c2 and c3 results in the empty relation
    Relation r2 = c.uniqueAbstract(home, {c1, c2});

    Relation expected2(home, {c0, c3});
    EXPECT_TRUE(r2.sameAs(expected2));
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(UniqueAbs, decimal) {
  // This is the same test as above but it uses decimal numbers directly.
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(4)), c1(home, makeDomain(4));
    Relation c(home, {c0, c1});

    c.add(home, {2, 0});
    c.add(home, {2, 1});
    c.add(home, {3, 0});
    c.add(home, {3, 1});

    // Abstracting c0 results in the empty relation
    Relation r = c.uniqueAbstract(home, {c0});

    Relation expected(home, {c1});
    EXPECT_TRUE(r.sameAs(expected));

    // Abstracting c2 results in the empty relation
    Relation r2 = c.uniqueAbstract(home, {c1});

    Relation expected2(home, {c0});
    EXPECT_TRUE(r2.sameAs(expected2));

    Relation d(home, {c0, c1});
    d.add(home, {0, 3});
    d.add(home, {1, 2});

    Relation e = c.unionWith(d);

    // abstracting c0 from e results in a relation with tuples {3} and {2} for
    // attribute c1
    Relation r3 = e.uniqueAbstract(home, {c0});

    Relation expected3(home, {c1});
    expected3.add(home, {3});
    expected3.add(home, {2});

    EXPECT_TRUE(r3.sameAs(expected3));

    // abstracting c1 from e results in a relation with tuples {0} and {1} for
    // attribute c0
    Relation r4 = e.uniqueAbstract(home, {c1});
    Relation expected4(home, {c0});
    expected4.add(home, {0});
    expected4.add(home, {1});
    EXPECT_TRUE(r4.sameAs(expected4));
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(UniqueAbs, abstractBinaryRelation) {
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(2)), c1(home, makeDomain(2));

    Relation c(home, {c0, c1});

    c.add(home, {1, 0});
    c.add(home, {0, 0});
    c.add(home, {1, 1});

    {
      // Abstracting at this point c0 uniquely will lead to a relation
      // containing {1}  for attribute c1.
      Relation r = c.uniqueAbstract(home, {c0});

      Relation expected(home, {c1});
      expected.add(home, {1});

      EXPECT_TRUE(expected.sameAs(r));
    }

    {
      // Abstracting at this point c1 uniquely will lead to a relation
      // containing
      // <0,0>.
      Relation r = c.uniqueAbstract(home, {c1});

      print(home, r, std::cout);
      Relation expected2(home, {c0});
      expected2.add(home, {0});
      EXPECT_TRUE(expected2.sameAs(r));
    }

    c.add(home, {0, 1});
    {
      // Abstracting either c0 or c1 will lead to empty relations as result
      Relation r = c.uniqueAbstract(home, {c1});

      // printRelation(home, r);
      Relation expected(home, {c0});
      EXPECT_TRUE(expected.sameAs(r));

      Relation r2 = c.uniqueAbstract(home, {c0});
      Relation expected2(home, {c1});
      EXPECT_TRUE(expected2.sameAs(r2));
    }
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(UniqueAbs, multiAttribute_Abstraction) {
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(2)), c1(home, makeDomain(3)),
        c2(home, makeDomain(4)), c3(home, makeDomain(2));

    Relation c(home, {c0, c1, c2, c3});

    c.add(home, {1, 2, 3, 0});
    c.add(home, {1, 1, 3, 1});
    c.add(home, {0, 1, 3, 0});
    c.add(home, {1, 1, 2, 1});

    Relation r = c.uniqueAbstract(home, {c0, c1});

    Relation expected(home, {c2, c3});
    expected.add(home, {2, 1});
    expected.add(home, {3, 1});

    print(home, r, std::cout);
    EXPECT_TRUE(r.sameAs(expected));
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(UniqueAbs, uniqueWithRespectTo) {
  SpaceManager home;
  {
    Attribute c0(home, makeDomain(256)), c1(home, makeDomain(256)),
        c2(home, makeDomain(256)), c3(home, makeDomain(256));

    Schema cDom(home, {c0, c1, c2, c3});
    Relation c(home, cDom);

    c.add(home, {0, 1, 2, 3});
    c.add(home, {4, 1, 2, 3});
    c.add(home, {9, 8, 2, 3});

    // Abstracting c0 results in the empty relation
    Relation r = c.uniqueWithRespectTo(home, {c1, c2, c3});
    Relation expected(home, cDom);
    expected.add(home, {9, 8, 2, 3});
    EXPECT_TRUE(r.sameAs(expected));
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(UniqueAbs, fast1) {
  SpaceManager home;
  {
    // The order of declaration in the attributes matters a lot for this test.
    // It guarantees that all the variables representing c0 are above the
    // variables representing c1.
    Attribute c0(home, makeDomain(4), false, "c0");
    Attribute c1(home, makeDomain(4), false, "c1");

    Relation c(home, {c0, c1});

    c.add(home, {2, 0});
    c.add(home, {2, 1});
    c.add(home, {3, 0});
    c.add(home, {3, 1});

    EXPECT_TRUE(c.canUseUniqueAbstractFast(home, {c1}));
    Relation res = c.uniqueAbstractFast(home, {c1});
    EXPECT_TRUE(res.isEmpty());

    c.add(home, {0, 2});
    c.add(home, {1, 3});

    res = c.uniqueAbstractFast(home, {c1});

    Relation d(home, {c0});
    d.add(home, {1});
    d.add(home, {0});

    EXPECT_TRUE(res.sameAs(d));
    //    print(home, res, std::cout);
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(UniqueAbs, somSimple) {
  SpaceManager home;
  {
    Attribute x(home, makeDomain(3), "x");
    Attribute y(home, makeDomain(8), "y");

    Relation r(home, {x, y});

    r.add(home, {1, 2});
    r.add(home, {1, 3});
    r.add(home, {1, 7});
    r.add(home, {2, 3});
    r.add(home, {2, 5});
    r.add(home, {0, 4});
    r.add(home, {1, 4});

    UniqueAbstractor ator = r.createUniqueAbstractor(home, {x});
    Relation result = r.uniqueWithRespectTo(home, ator);
    Relation expected(home, {x, y});
    expected.add(home, {0, 4});
    EXPECT_TRUE(result.sameAs(expected));
    // print(home, result, std::cout);
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(UniqueAbs, somMultiAttribute) {
  SpaceManager home;
  {
    Attribute a(home, makeDomain(4), "a");
    Attribute b(home, makeDomain(4), "b");
    Attribute c(home, makeDomain(4), "c");
    Attribute d(home, makeDomain(4), "d");

    Relation r(home, {a, b, c, d});

    r.add(home, {1, 2, 3, 0});
    r.add(home, {1, 1, 3, 1});
    r.add(home, {0, 1, 3, 0});
    r.add(home, {1, 1, 2, 1});

    UniqueAbstractor ator = r.createUniqueAbstractor(home, {c, d});
    Relation result = r.uniqueWithRespectTo(home, ator);

    Relation expected(home, {a, b, c, d});
    expected.add(home, {1, 1, 3, 1});
    expected.add(home, {1, 1, 2, 1});
    EXPECT_TRUE(result.sameAs(expected));
    // print(home, result, std::cout);
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(UniqueAbs, somMultiAttributeDifferentReprSize) {
  SpaceManager home;
  {
    Attribute a(home, makeDomain(4), "a");
    Attribute b(home, makeDomain(4), "b");
    Attribute c(home, makeDomain(4), "c");
    Attribute d(home, makeDomain(4), "d");

    Relation r(home, {a, b, c, d});

    r.add(home, {1, 2, 3, 0});
    r.add(home, {1, 1, 3, 1});
    r.add(home, {0, 1, 3, 0});
    r.add(home, {1, 1, 2, 1});

    UniqueAbstractor ator = r.createUniqueAbstractor(home, {c, d});
    Relation result = r.uniqueWithRespectTo(home, ator);

    Relation expected(home, {a, b, c, d});
    expected.add(home, {1, 1, 3, 1});
    expected.add(home, {1, 1, 2, 1});
    EXPECT_TRUE(result.sameAs(expected));
    // print(home, result, std::cout);
  }
  EXPECT_EQ(0, home.zeroReferences());
}