#include <iostream>
#include <gtest/gtest.h>

#include "bdd-abstraction.hh"

using namespace std;
using namespace CuddAbstraction;

TEST(Misc, wikiExample) {
  SpaceManager home;
  {
    Attribute x1(home, makeDomain(2), "x_1"), x2(home, makeDomain(2), "x_2"),
        x3(home, makeDomain(2), "x_3");
    Schema sch(home, {x1, x2, x3});
    Relation r(home, sch);
    r.add(home, {0, 0, 0});
    r.add(home, {0, 1, 1});
    r.add(home, {1, 1, 0});
    r.add(home, {1, 1, 1});

    r.toDot(home, "wikiExample.dot", "f");
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Misc, encodingExample) {
  SpaceManager home;
  {
    Attribute a(home, makeDomain(2), "A"), b(home, makeDomain(8), "B"),
        c(home, makeDomain(16), "C");
    Schema sch(home, {a, b, c});
    Relation r(home, sch);
    r.add(home, {1, 5, 10});
    r.add(home, {0, 6, 12});
    r.add(home, {1, 7, 15});
    // r.add(home, {1, 5, 12});
    // r.add(home, {1, 5, 15});
    // r.add(home, {1, 7, 12});
    // r.add(home, {0, 7, 12});

    r.toDot(home, "encodingExample.dot", "f'");
    r.schema().toDot(home, "schemaEncodingExample.dot", "s");

    RelationStats stats(r);
    std::cout << stats;
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Misc, encodingExampleFixedOrder) {
  SpaceManager home;
  {
    // Variables used in the representation
    // A {0} total: 1
    // B {1,3,5} total: 3
    // C {2,4,6,7} total: 4
    Attribute a(home, {0}, makeDomain(2), "A"),
        b(home, {1, 3, 5}, makeDomain(8), "B"),
        c(home, {2, 4, 6, 7}, makeDomain(16), "C");
    Schema sch(home, {a, b, c});
    Relation r(home, sch);
    r.add(home, {1, 5, 10});
    r.add(home, {0, 6, 12});
    r.add(home, {1, 7, 15});
    // r.add(home, {1, 5, 12});
    // r.add(home, {1, 5, 15});
    // r.add(home, {1, 7, 12});
    // r.add(home, {0, 7, 12});

    r.toDot(home, "interleavedEncodingExample.dot", "f'");

    RelationStats stats(r);
    std::cout << stats;

    home.ReduceHeap(CUDD_REORDER_SYMM_SIFT_CONV, 0);

    r.toDot(home, "interleavedEncodingExampleAfterReorder.dot", "f'");

    RelationStats stats2(r);
    std::cout << stats2;
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Misc, encodingAggregationExample) {
  SpaceManager home;
  {
    // The idea of this test is to produce an ADD output for the following
    // aggregated relation:
    // =================
    // A & B & C  & Agg
    // =================
    // 1 & 5 & 10 & 2
    // 0 & 6 & 12 & 3
    // 1 & 7 & 15 & 1
    // =================

    // At this time I have no API to build it other than group by. For that
    // reason I actually encode the relation:
    // =================
    // A & B & C  & D
    // =================
    // 1 & 5 & 10 & 0
    // 1 & 5 & 10 & 1
    // 0 & 6 & 12 & 0
    // 0 & 6 & 12 & 1
    // 0 & 6 & 12 & 2
    // 1 & 7 & 15 & 0
    // =================

    //
    // Variables used in the representation
    // A {0} total: 1
    // B {1,3,5} total: 3
    // C {2,4,6,7} total: 4
    // D {8,9} total: 2
    Attribute a(home, makeDomain(2), "A"), b(home, makeDomain(8), "B"),
        c(home, makeDomain(16), "C"), d(home, makeDomain(4), "D");
    Schema sch(home, {a, b, c, d});
    Relation r(home, sch);
    r.add(home, {1, 5, 10, 0});
    r.add(home, {1, 5, 10, 1});
    r.add(home, {0, 6, 12, 0});
    r.add(home, {0, 6, 12, 1});
    r.add(home, {0, 6, 12, 2});
    r.add(home, {1, 7, 15, 0});

    AggRelation group = AggRelation::groupBy(home, r, {a, b, c});
    group.toDot(home, "aggregatedEncodingExample.dot", "G");
    // r.toDot(home, "interleavedEncodingExample.dot", "f'");
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Misc, encodingGroupByObjectExample) {
  SpaceManager home;
  {
    Attribute a(home, makeDomain(2), "A"), b(home, makeDomain(2), "B"),
        c(home, makeDomain(2), "C"), d(home, makeDomain(2), "D");
    Schema sch(home, {a, b, c, d});
    Relation r(home, sch);
    r.add(home, {1, 0, 1, 1});
    r.add(home, {1, 1, 1, 1});
    r.add(home, {0, 0, 1, 1});

    r.add(home, {1, 0, 1, 0});
    r.add(home, {1, 1, 1, 0});

    r.add(home, {0, 0, 0, 0});

    AggRelation group = AggRelation::groupBy(home, r, {c, d});
    group.toDot(home, "groupByObjectExample.dot", "G");
  }
  EXPECT_EQ(0, home.zeroReferences());
}

TEST(Misc, groupInterval) {
  SpaceManager home;
  {
    Attribute a(home, makeDomain(4), "A");
    Attribute b(home, makeDomain(4), "B");
    Attribute c(home, makeDomain(2), "C");

    Schema abc(home, {a, b, c});
    Relation r(home, abc);

    r.add(home, 2, 1, 1);
    r.add(home, 2, 2, 1);
    r.add(home, 2, 3, 1);
    r.add(home, 1, 0, 1);
    r.add(home, 1, 1, 1);
    r.add(home, 0, 0, 0);

    AggRelation g = AggRelation::groupBy(home, r, {a, c});
    g.toDot(home, "groupIntervalExampleGroup.dot", "G");

    Relation sol = g.filterInterval(home, 2, 3);
    sol.toDot(home, "groupIntervalExampleResult.dot", "F");
  }
}

TEST(Misc, groundEQ) {
  SpaceManager home;
  {
    // Create two non-interleaved attributes
    Attribute x(home, makeDomain(8), false, "x");
    Attribute y(home, makeDomain(8), false, "y");

    Schema schX(home, {x});
    Schema schY(home, {y});
    BDD eq = UniqueAbstractor::buildEquality(home, schX, schY);

    Schema sch(home, {x, y});
    auto levels = sch.levelNames(home);
    home.toDot("eqlinear.dot", eq.Add().getNode(), "EQ", levels);
  }
  {
    // Create two interleaved attributes
    Attribute x(home, makeDomain(8), true, "x");
    Attribute y(home, makeDomain(8), true, "y");

    Schema schX(home, {x});
    Schema schY(home, {y});
    BDD eq = UniqueAbstractor::buildEquality(home, schX, schY);

    Schema sch(home, {x, y});
    auto levels = sch.levelNames(home);
    home.toDot("eqinterleaved.dot", eq.Add().getNode(), "EQ", levels);
  }
  EXPECT_EQ(0, home.zeroReferences());
}
