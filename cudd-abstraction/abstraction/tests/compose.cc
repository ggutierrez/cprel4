#include <iostream>
#include <gtest/gtest.h>

#include "bdd-abstraction.hh"

using namespace std;
using namespace CuddAbstraction;

TEST(RelOperation, simpleCompose) {
  SpaceManager home;
  {

    Attribute a(home, makeDomain(4), "a"), b(home, makeDomain(4), "b"),
        i(home, makeDomain(4), "i");
    Schema xDom(home, {a, i}), yDom(home, {i, b}), zDom(home, {a, b});

    Relation x(home, xDom);
    x.add(home, 1, 0);
    x.add(home, 2, 0);
    x.add(home, 2, 3);

    Relation y(home, yDom);
    y.add(home, 0, 2);
    y.add(home, 3, 0);
    y.add(home, 3, 1);

    Relation z = y.composeWith(home, x);

    EXPECT_TRUE(z.schema().sameAs(zDom));

    Relation result(home, zDom);
    result.add(home, 1, 2);
    result.add(home, 2, 2);
    result.add(home, 2, 0);
    result.add(home, 2, 1);

    EXPECT_TRUE(result.sameAs(z));

    // printRelation(home,x);
    // printRelation(home,y);
    // printRelation(home,z);
  }
  EXPECT_EQ(0, home.zeroReferences());
}
