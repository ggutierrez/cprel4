#ifndef __ATTRIBUTE_MAPPING_HH
#define __ATTRIBUTE_MAPPING_HH

#include "attribute-mapping-decl.hh"
#include <cassert>

namespace CuddAbstraction {

inline AttributeMap::AttributeMap(void) {}

inline AttributeMap::AttributeMap(
    const std::initializer_list<std::pair<Attribute, Attribute>>& m) {
  for (const std::pair<Attribute, Attribute>& entry : m)
    attributeMap_.insert(MapEntry(entry.first, entry.second));
}

inline AttributeMap::AttributeMap(const AttributeMap& m)
    : attributeMap_(m.attributeMap_) {}

inline AttributeMap::~AttributeMap(void) {}

inline size_t AttributeMap::size(void) const { return attributeMap_.size(); }

inline void AttributeMap::add(const Attribute& a0, const Attribute a1) {
  attributeMap_.insert(MapEntry(a0, a1));
}

inline bool AttributeMap::isInLHS(const Attribute& a) const {
  return attributeMap_.by<lhs>().find(a) != attributeMap_.by<lhs>().end();
}

inline bool AttributeMap::isInRHS(const Attribute& a) const {
  return attributeMap_.by<rhs>().find(a) != attributeMap_.by<rhs>().end();
}

inline const Attribute& AttributeMap::getRHSOf(const Attribute& a) const {
  auto it = attributeMap_.by<lhs>().find(a);
  assert((it != attributeMap_.by<lhs>().end()) &&
         "Attribute is not part of the map's LHS");
  return it->second;
}

inline const Attribute& AttributeMap::getLHSOf(const Attribute& a) const {
  auto it = attributeMap_.by<rhs>().find(a);
  assert((it != attributeMap_.by<rhs>().end()) &&
         "Attribute is not part of the map's LHS");
  return it->second;
}

inline void AttributeMap::apply(
    std::function<void(const Attribute& l, const Attribute& r)> fun) const {
  for (const auto& entry : attributeMap_.by<lhs>())
    fun(entry.first, entry.second);
}

inline AttributeMap AttributeMap::inverse(void) const {
  AttributeMap inv;
  for (const auto& entry : attributeMap_.by<lhs>())
    inv.add(entry.second, entry.first);
  return inv;
}

inline bool AttributeMap::isIdentity(void) const {
  for (const auto& entry : attributeMap_.by<lhs>())
    if (entry.first != entry.second)
      return false;
  return true;
}

inline bool AttributeMap::mapsAllAttributesIn(const Schema& s) const {
  if (size() != s.arity())
    return false;

  for (const Attribute& att : s.asColumnDomains())
    if (!isInLHS(att))
      return false;
  return true;
}

inline bool AttributeMap::mappingBetween(const Schema& s0,
                                         const Schema& s1) const {
  if ((size() != s0.arity()) || (size() != s1.arity()))
    return false;

  for (const Attribute& att : s0.asColumnDomains())
    if (!isInLHS(att))
      return false;
  for (const Attribute& att : s1.asColumnDomains())
    if (!isInRHS(att))
      return false;
  return true;
}

inline std::pair<std::vector<BDD>, std::vector<BDD>>
AttributeMap::BDDMapping(const SpaceManager& home) const {
  // TODO: It would be nice to reserve the right number of elements from the
  // declaration point.
  std::vector<BDD> left, right;
  for (const auto& entry : attributeMap_.by<lhs>()) {
    const Attribute& lhsAtt = entry.first;
    const Attribute& rhsAtt = entry.second;
    if (!lhsAtt.hasSameAttributeDomainAs(rhsAtt))
      throw InvalidAttributeDomainMapping("AttributeMap::BDDMapping");
    // TODO: The following loop can be simplified by using the method
    // asVectorOfVariables in the Attribute clas.
    for (size_t i = 0; i < lhsAtt.reprSize(); i++) {
      left.push_back(home.bddVar(lhsAtt.getIndices()[i]));
      right.push_back(home.bddVar(rhsAtt.getIndices()[i]));
    }
  }
  return std::make_pair(std::move(left), std::move(right));
}

inline std::ostream& operator<<(std::ostream& os, const AttributeMap& m) {
  m.apply([&](const Attribute& l, const Attribute& r) {
    os << l << " <--> " << r;
  });
  return os;
}
}

#endif
