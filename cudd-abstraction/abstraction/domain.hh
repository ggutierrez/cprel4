#ifndef __DOMAIN_HH__
#define __DOMAIN_HH__

#include "attribute-decl.hh"

namespace CuddAbstraction {
////////////////
// BaseDomain //
////////////////
inline BaseDomain::BaseDomain(void) : max_cardinality_(0) {}

inline BaseDomain::BaseDomain(const IntegerDatum& size)
    : max_cardinality_(size) {
  assert((size == 0 || Limits::valid(size - 1)) &&
         "Invalid domain cardinality");
}

inline BaseDomain::~BaseDomain(void) {}

inline size_t BaseDomain::maxCardinality(void) const {
  return max_cardinality_;
}

inline int BaseDomain::max(void) const { return max_cardinality_ - 1; }

inline bool BaseDomain::valid(int v) const {
  return Limits::valid(v) && v <= max();
}

inline size_t BaseDomain::printWidth(void) const {
  return std::to_string(max()).length();
}

inline AttributeDomain makeDomain(int card) {
  if (card != 0 && !Limits::valid(card - 1))
    throw InvalidDomainData("makeDomain");
  return std::make_shared<BaseDomain>(card);
}
}

#endif