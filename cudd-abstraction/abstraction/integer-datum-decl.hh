#ifndef __INTEGER_DATUM_DECL_HH__
#define __INTEGER_DATUM_DECL_HH__

#include <bitset>
#include <cassert>
#include <iostream>
#include <iterator>
#include <limits>
#include <stdio.h>
#include <string>
#include <vector>

namespace CuddAbstraction {
namespace Limits {
/// Minimum integer data that can encode the value of an attribute
static const int min = 0;
/// Maximum integer data that can encode the value of an attribute
static const int max = std::numeric_limits<int>::max();
/// Check if @a n is a valid encoding for an attribute value
inline constexpr bool valid(int n) { return n >= min && n <= max; }
inline constexpr size_t max_bits() { return std::numeric_limits<int>::digits; }
/**
 * @brief Tests whether @a i represent a valid bit of the encoding of an
 *     attribute.
 */
inline constexpr bool valid_bit(int i) {
  return (i >= 0) && (static_cast<size_t>(i) < max_bits());
}
/**
 * @brief Tests whether the underlying representation can represent numbers of
 * @a k bits.
 */
inline constexpr bool canRepresent(size_t k) { return k <= max_bits(); }
/**
 * @brief Returns the number of bits required to represent @a n
 *
 * \warning Assumes @a n is a valid number to encode.
 */
inline size_t bitsRequiredToRepresent(int n) {
  if (n == 0)
    return 1;

  unsigned int d = n;
  size_t numBits = 0;
  while (d) {
    numBits++;
    d >>= 1;
  }
  return numBits;
}
} // Limits
} // CuddAbstraction

#define ABSTRACTION_VALID(n) assert(CuddAbstraction::Limits::valid((n)));

namespace CuddAbstraction {
/**
 * @brief Representation for positive integers
 *
 * All the elements that can be part of a tuple in a relation are positive
 * integers. All the integers that can be part of a tuple must be in the range
 * [0,Limits::max]. Limits::max is chosen in a way that it can be always stored
 * in an int.
 */
class IntegerDatum {
private:
  /**
   * @brief Storage for the representation for positive integers
   *
   * The underlying data structure used to represent the integer is long enough
   * to store only positive integers.
   */
  std::bitset<Limits::max_bits()> storage_;

public:
  /**
   * @brief Constructors
   */
  ///@{
  /// Default constructor
  IntegerDatum(void);
  /// Constructor from an integer x
  IntegerDatum(int x);
  /// Copy constructor
  IntegerDatum(const IntegerDatum& x);
  /// Destructor
  ~IntegerDatum(void);
  ///@}
  /**
   * @brief Representation operations
   */
  ///@{
  /// Number of bits required to represent this number
  size_t numBits(void) const;
  /// Returns the state of the n-th bit
  bool getBit(int n) const;
  /// Sets the n-th bit
  void setBit(int n);
  /// Clears the n-th bit
  void clearBit(int n);
  ///@}
  std::string encoding(void) const;
  /// Converts an integer datum to a native integer
  operator int(void) const;
  /// Less than or equal operator
  bool operator<=(const IntegerDatum& n) const;
};

/// IntegerDatum output
std::ostream& operator<<(std::ostream& os, const IntegerDatum& d);

/// Type definition for a tuple of IntegerDatums represented as a vector.
typedef std::vector<IntegerDatum> NumericTuple;

/// Numeric tuple output
std::ostream& operator<<(std::ostream& os, const NumericTuple& t);
}

/**
 * @brief Encodes @a d and returns the result as an string of bits.
 * 
 * This is a reasonably simple way of making IntegerDatum encoding available to
 * C functions. The string contains 0 to represent the false bits and 1 for the
 * true ones. First character in the string corresponds to the N-1 bit and the
 * last one to the first bit.
 *     
 * @warn The callee is in charge of the disposal of the returned string.
 */
extern "C" char* encodeInteger(int d);

#endif
