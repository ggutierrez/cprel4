#ifndef __SET_SPACE_DECL_HH__
#define __SET_SPACE_DECL_HH__

#include <cuddObj.hh>
#include "exception-decl.hh"

#include <bitset>
#include <functional>
#include <set>
#include <string>
#include <unordered_map>

namespace CuddAbstraction {

/**
 * @brief Exception thrown on invalid allocation.
 */
class InvalidAllocation : public Exception {
public:
  InvalidAllocation(void) = delete;
  InvalidAllocation(const char* location)
      : Exception(location,
                  "Attempt to allocate BDD variables resulted in an error") {}
};

/**
 * @brief Exception thrown when the allocator is out of space.
 */
class FailedAllocation : public Exception {
public:
  FailedAllocation(void) = delete;
  FailedAllocation(const char* location)
      : Exception(location, "Not enough BDD variables to allocate from") {}
};

/**
 * @brief A variable allocator is in charge of managing the variables of the BDD
 *     manager.
 *
 * This class is a thin interface to the CUDD manager. It aims at providing
 * primitives that allow to request (i.e. allocate) variables from the manager
 * in a way that is sensible to relation representations.
 *
 * Particular BDD representations benefit from diferent orderings. Two of them
 * being interleaved and linear. To support both cases, the allocator has two
 * areas: linear and interleaved.
 *
 * - Interleaved: consists of a group of variables that form variable blocks.
 *   One an allocation of @a k variables is performed in this region we use
 *   one unused variable from each block. All blocks are of @a blockSize
 *   variables and there are @a numBlocks blocks.
 *
 * - Linear: for linear allocation, a group of different variables is used.
 *   This group is treated as an array and the allocations will reserve
 *   consecutive variables. Two allocations from this region will provide
 *   variables with smaller indices than the second one.
 *
 * @todo The allocator currently lacks operations to *free* variables.
 *
 * @todo There can only be one manager instance but this is not checked.
 *     However, the only place where the manager is used is by the SpaceManager.
 */
class VarAllocator {
private:
  /**
   * @brief Number of BDD variables that the allocator will manage.
   */
  static constexpr int ALLOCATOR_SIZE = CUDD_MAXINDEX + 1;
  /**
   * @brief Type definition for the free variables container.
   */
  using FreeLeist = std::bitset<ALLOCATOR_SIZE>;
  /**
   * @brief Storage representing the state (used/unused) of the BDD variables.
   *
   * Variable i is available (not used) if  @a freeVars at position i is true.
   */
  FreeLeist freeVars_;
  /**
   * @brief Size of each block in the interleaved region
   */
  const size_t blockSize;
  /**
   * @brief Number of blocks used for the interleaved region.
   */
  const size_t numBlocks;

public:
  /// Constructor
  VarAllocator(size_t bs = 128, size_t nb = 128)
      : blockSize(bs)
      , numBlocks(nb) {
    // All variables start free (i.e. unused)
    freeVars_.set();
  }

private:
  /// Returns the index of the first variable in the linear region.
  size_t linearStart() const;
  /// Marks variable at @a index as allocated.
  void markAllocated(size_t index);
  /// Returns the block of BDD variable @a index.
  size_t blockOf(size_t index) const;
  /// Returns the first BDD variable available in block @a block.
  size_t nextAvailableInBlock(size_t block) const;
  /// Allocates @a length BDD variable from the interleaved region
  std::vector<int> allocInterleaved(size_t length);
  /// Allocates @a length BDD variable from the linear region
  std::vector<int> allocLinear(size_t length);

public:
  /**
   * @brief Allocates @a length BDD variables.
   *
   * If @a interleaved is set to true, the variables are allocated from the
   * interleaved region.
   *
   * TODO: Release a variable is not possible in the current version but is a
   * functionality that might be desirable.
   */
  std::vector<int> alloc(size_t length, bool interleaved);

  /**
   * @brief Allocates the variables specified in @a vars.
   *
   * When calling this method make sure you know what you are doing. The only
   * purpose of this method is to give the freedom for upper layers of the
   * software to decide which BDD variables will be used for representing
   * particular entities.
   *
   * Any variable that is unused is marked as used in such a way that calls to
   * other methods can return truly new domains. If a used variable is in @a
   * vars is fine, it will be reused.
   */
  std::vector<int> alloc(const std::vector<int>& vars);

public:
  /**
   * @brief Prints debug information about this allocator.
   */
  void printDebugInfo(void) const;
};

/**
 * @brief Exception thrown when values required to be power of two are not.
 */
class InvalidPowerOfTwoValue : public Exception {
public:
  InvalidPowerOfTwoValue(void) = delete;
  InvalidPowerOfTwoValue(const char* location)
      : Exception(location, "Invalid power of two argument") {}
};

/**
 * @brief Management of BDD variables memory space.
 * @details This class provides an interface to the BDD operations that require
 *     a BDD manager. It tries to encapsulate all the BDD internal operations
 *     required by the abstraction module.
 *
 */
class SpaceManager : public Cudd {
  ////////////////
  // Attributes //
  ////////////////
private:
  /**
   * @brief Allocator allocator to the manager
   *
   * The template parameter is chosen in such a way that the allocator will
   * manage all the BDD variables that could be offered by the BDD library
   */
  VarAllocator allocator_;

  /**
   * @brief Number of times that unique abstraction has been called.
   */
  mutable int uniqueAbstractCalls_;
  /**
   * @brief Number of times that the fast unique abstraction has been called.
   *
   * A fast unique abstraction counts as a unique abstraction operation. For
   * that reason both this counter and @a uniqueAbstractCalls_ are incremented
   * when a fast operation is possible.
   */
  mutable int fastUniqueAbstractCalls_;
  /**
   * @brief Whether display manager statistics on destruction
   */
  bool statsOnDestruction_;

public:
  /**
   * @brief Type definition for the variables representing a relation domain
   */
  typedef std::vector<int> BDDVarDomain;

  ////////////////////////////////
  // Constructor and destructor //
  ////////////////////////////////
public:
  // Prevent assignment
  SpaceManager& operator=(const SpaceManager&) = delete;
  // Prevent copy construction
  SpaceManager(const SpaceManager&) = delete;
  /**
   * @brief Constructs a BDD manager with the respective parameters.
   *
   * @param statsOnDestruction: Whether to print BDD usage statistics on manager
   *     destruction.
   * @param slotsInUniqueTable: Number of initial slots in the unique table.
   * @param  slotsInCache: Number of slots in the cache.
   * @param  maxMemory Maximum memory (i.e. 256 * MemSize::MB) to use a maximum
   *     of 256 Mega bytes. Use 0 for CUDD default.
   * @param cacheMinEff: Minimum efficiency for cache resizing (use 0 for CUDD
   *     default).
   * @param maxSlotsInCache: hard limit for the maximum number of entries in the
   *     cache.
   * @param dynamicReorder: Whether to enable or not dynamic reordering. In case
   *     of being true the reordering method is set to CUDD_REORDER_SYMM_SIFT
   *
   *
   * @warn Throws an exception @a InvalidPowerOfTwoValue if @a
   *     slotsInUniqueTable or @a slotsInCache are not powers of two.
   */
  SpaceManager(bool statsOnDestruction = false,
               unsigned int slotsInUniqueTable = CUDD_UNIQUE_SLOTS,
               unsigned int slotsInCache = CUDD_CACHE_SLOTS,
               unsigned long maxMemory = 0, unsigned int cacheMinEfficiency = 0,
               unsigned int maxSlotsInCache = 0, bool dynamicReorder = false);
  /// Destructor
  virtual ~SpaceManager(void);

  //////////////////////////
  // Basic BDD operations //
  //////////////////////////
public:
  /// \name Basic BDD operations
  //@{
  /// Returns the level (or variable index) associated to @a d
  int level(DdNode* d) const;
  /// Returns the _then_ subgraph of BDD @a r
  BDD high(const BDD& r) const;
  /// Returns the _else_ subgraph of BDD @a r
  BDD low(const BDD& r) const;
  /// Returns the regular (i.e. not complemented) form of r
  BDD regular(const BDD& r) const;
  /// Returns the level of @a r
  unsigned int var2level(const BDD& r) const;
  /// Returns the top variable of @a r
  BDD topVar(const BDD& r) const;
  /// Returns the value stored by the constant node @a r
  int value(const ADD& r) const;
  //@}
  /// \name Extra BDD operations
  //@{
  /**
   * @brief Tests if two BDD's @a r and @a s have variables in common
   */
  static bool haveVarsInCommon(const BDD& r, const BDD& s);
  /**
   * @brief Tests if all the variables of BDD @a r are variables in BDD @a s
   */
  static bool variablesSubset(const BDD& r, const BDD& s);
  /**
   * @brief Tests if BDD @a r is terminal false
   */
  static bool isFalse(const BDD& r);
  /**
   * @brief Tests if BDD @a r is terminal false
   */
  static bool isTrue(const BDD& r);
  /**
   * @brief Given a cube of variables (@a c) returns a vector with each variable
   *     in it.
   */
  std::vector<BDD> cubeToVector(BDD c) const;
  /**
   * @brief Print the levels of the variables in @a cube to stdout.
   */
  void printLevels(BDD cube) const;
  //@}

  ////////////////
  // Allocation //
  ////////////////
public:
  /**
   * Allocates a new domain of @a length BDD variables.
   *
   * If @a interleaved is set to true then the variables are guaranted to be
   * interleaved with any other interleaved allocation.
   */
  BDDVarDomain alloc(size_t length, bool interleaved = true);
  /// Allocates a new domain with the BDD variables in @a vars
  BDDVarDomain alloc(const std::vector<int>& vars);
  /////////////////
  // Information //
  /////////////////
public:
  /// Print manager information to \a os
  void info(std::ostream& os) const;
  /**
   * @brief Return the number of current zero references
   *
   * This method might be used by new operations to check that they manage the
   * reference counting appropriately.
   */
  int zeroReferences(void) const;

  ////////////////////////
  // Unique abstraction //
  ////////////////////////
public:
  /**
   * @brief Unique quantification of @a f with respect to @a cube
   */
  BDD uniqueAbstract(BDD f, BDD cube) const;
  /**
   * @brief Unique quantification of @a f with respect to @a cube.
   *
   * The following requirement must be satisfied prior to the application of
   * this method. The non abstracted variables of @a f must be above the
   * variables of @a cube. In order to check for this condition use
   * @a canUseUniqueAbstractFast
   */
  BDD uniqueAbstractFast(BDD f, BDD cube) const;
  /**
   * @brief Tests if it is possible to apply fast unique abstraction of a cube
   * @a toAbstract when the non abstracted variables are represented by
   * @a notAbstracted.
   */
  bool canUseUniqueAbstractFast(BDD notAbstracted, BDD toAbstract) const;

  ///////////////////////////////////
  // MTBDDs (Aggregated relations) //
  ///////////////////////////////////
public:
  /**
   * @brief Find the minimum discriminant different from 0 of @a f
   */
  ADD findMinNonzero(ADD f) const;
  /**
   * @brief Returns the BDD resulting from encoding the discriminants of @a a
   *     using the variable indexes @a vars.
   *
   * @a cube is the cube representing the variables in @a indices
   */
  BDD augmentedWithDiscriminants(ADD a, BDD cube,
                                 const BDDVarDomain& indices) const;

  //////////////////////
  // BDD Output (dot) //
  //////////////////////
public:
  /**
   * Type definition for a map that associates levels in a BDD to strings.
   */
  typedef std::unordered_map<int, std::string> LevelNamesMapType;
  /**
   * @brief Scans the BDD @a f and returns the levels of all the nodes that
   *     appear on it.
   *
   * The value associated to each level is an empty string.
   */
  LevelNamesMapType extractLevels(DdNode* f) const;
  /**
   * @brief Creates a dot representation of @a f and outputs it on @a os.
   *
   * - @a width and @a height specify the dimensions after which the graph is
   *   scaled.
   */
  void toDot(std::ostream& os, DdNode* f, const std::string& funcName,
             const LevelNamesMapType& names, float width = 7.5,
             float height = 10.0) const;
  /**
   * @brief Creates a dot representation of @a f in a file with name @a
   *     fileName.
   *
   * - @a width and @a height specify the dimensions after which the graph is
   *   scaled.
   */
  void toDot(const std::string& fileName, DdNode* f,
             const std::string& funcName, const LevelNamesMapType& names,
             float width = 7.5, float height = 10.0) const;

  ////////////////
  // Statistics //
  ////////////////
public:
  /**
   * @brief Returns the memory (MB.) currently used by the BDD manager.
   */
  unsigned long memoryInUse() const;
  /**
   * @brief Returns the number of accesses to the cache.
   */
  double cacheLookUps() const;
  /**
   * @brief Returns the cache efficiency.
   *
   * The efficiency is measured by dividing the number of cache hits on the
   * number of cache lookups.
   */
  double cacheEfficiency() const;
  /**
   * @brief Returns the nuber of garbage collection executions.
   */
  int garbageCollections() const;
  /**
   * @brief Returns the time (in milliseconds) spent in garbage collection.
   *
   */
  long garbageCollectionTime() const;
  /**
   * @brief Returns the number of calls to unique abstraction.
   *
   * This does include fast unique abstractions.
   */
  int uniqueAbstractCalls() const;
  /**
   * @brief Returns the number of calls to fast unique abstraction.
   */
  int fastUniqueAbstractCalls() const;
  /**
   * @brief Returns the number of low-level calls to unique abstraction.
   *
   * This counter includes recursive calls of the implementation itself.
   */
  unsigned long uniqueAbstractRecCalls() const;
  /**
   * @brief Returns the number of low-level calls to fast unique abstraction.
   *
   * This counter includes recursive calls of the implementation itself.
   */
  unsigned long fatsUniqueAbstractRecCalls() const;
  // TODO: Missing documentation of the following operations
  unsigned long existAbstractRecCalls() const;
  unsigned long uniqueExistAbstractRecCalls() const;
  unsigned long iteRecCalls() const;

  /*
// TODO: Review this and see if there is a missing stat.
  printf("\tTotal calls to fast unique abstraction: %lu\n",
         manager->fast_unique_abstraction_calls);
   */
};
/**
 * @brief Type definition for a node visitor
 */
typedef std::function<void(DdNode* n)> NodeVisitor;
/**
 * @brief Visit all the nodes of @a f and apply @a visit
 *
 */
void visitBDDNodes(DdNode* f, NodeVisitor visit);
/**
 * \brief Iterates all the cubes of \a f and calls visitor function \a v on
 * each of them.
 *
 * \a v is a function that takes as arguments an array of integers. This array
 * maps a BDD variable index to the actual value of the variable in \a f.
 *
 * In the array, each element can be:
 * - 0 : The variable is false
 * - 1 : The variable is true
 * - 2 : The variable is a don't care (it can have both values)
 */
void cubeVisitor(BDD f, std::function<void(int*)> v);
/**
 * Given a minterm \a minterm along with its size \a size, compute all the
 * possible valuations of \a minterm and call the function \a v on each of them.
 *
 * - A minterm is an array of integers with the following conventions:
 *   - minterm[i] = 0 : the BDD variable with index \a i is zero in the minterm
 *   - minterm[i] = 1 : the BDD variable with index \a i is one in the minterm
 *   - minterm[i] = 2 : the BDD variable with index \a i has both values in the
 *     minterm
 *
 * - The minterm is indexed by the index of a variable in the manager.
 */
void assignementVisitor(int* minterm, unsigned int size,
                        const std::set<unsigned int>& filter,
                        std::function<void(int*)> visit,
                        unsigned int current = 0);

/**
 * @brief Exception thrown on an unexpected value returned by a CUDD
 *     operation.
 */
class CUDDUnexpected : public Exception {
public:
  CUDDUnexpected(void) = delete;
  CUDDUnexpected(const char* location)
      : Exception(location, "Generic exception fix-me") {}
};
}

#endif
