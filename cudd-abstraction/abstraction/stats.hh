#ifndef __RELATION_STATS_HH__
#define __RELATION_STATS_HH__

#include "stats-decl.hh"

namespace CuddAbstraction {

inline BDD RelationStats::asBdd(void) const { return rel_.data_; }

inline int RelationStats::nodes(void) const { return asBdd().nodeCount(); }

inline double RelationStats::density(void) const {
  auto nodes = asBdd().nodeCount();
  auto card = rel_.cardinality();
  return card / nodes;
}

inline double RelationStats::cardinality(void) const {
  return rel_.cardinality();
}

inline int RelationStats::sharingSize(const SpaceManager& home,
                                      const std::vector<Relation>& rels) {
  int totalNodes = 0;
  std::vector<BDD> reprs;
  reprs.reserve(rels.size());
  for (const Relation& r : rels) {
    reprs.push_back(r.data_);
    totalNodes += r.data_.nodeCount();
  }
  return totalNodes - home.SharingSize(reprs);
}

inline std::ostream& operator<<(std::ostream& os, const RelationStats& st) {
  os << "\tNodes: " << st.nodes() << "\n"
     << "\tDensity: " << st.density() << "\n"
     << "\tCardinality: " << st.cardinality() << "\n";
  return os;
}
}

#endif