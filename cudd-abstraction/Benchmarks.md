# Sqlite

Some sqlite configuration:
```
.mode column
.header on
```

To redirect the output of a query to a file:
```
.output file.dat
```

# Gnuplot
Homebrew installation of gnuplot
```
brew install gnuplot --qt --wx
```

Terminal type:
```
set term qt
```

To plot data coming from a query we have to set the separator
```
set datafile separator "|"
```

Variables affecting the plot
```
set title 'This is the title'
set xlabel 'Goes on x axis'
set ylabel 'Goes on y axis'
set xticks *start* *increment*
```

# Views for statistics
```sql
create view timestats as 
  select operations.name, times.time 
  from operations, times 
  where operations.opid = times.opid;
```
# Queries

Returns each operation and the total amount of time it took to run.
```sql
select name, sum(time) from timestats group by name order by sum(time) desc;
```

# Cleaner data
I use this sed command to cleanup the data

```bash
sed 's/.*Relation\:\://g' times.dat | sed 's/(.*\|/\|/g' query_result.dat > toplot.dat
```
```gnuplot
set terminal qt
set title "Benchmark PIP solver"
set xtics rotate
plot "toplot.dat" using 2:xtic(1)
```