# Relation Decision Variables implementation

## Requirements

  - CMake version 3.2 or above
  - Recent C++ compiler with c++11 support.

## Project checkout
Assume _workdir_ is the name of the folder you are currently in. To fetch the 
sources of this project type in a terminal:

```
workdir$ git clone --recursive https://bitbucket.org/ggutierrez/cprel4.git
```

That will create a directory called _cprel4_.

## Build instructions

Inside _workdir_ folder create a directory called _build-cprel4_.

```
workdir$ mkdir build-cprel4
workdir$ cd build-cprel4
build-cprel4$ cmake -DCMAKE_BUILD_TYPE=Release ../cprel4
build-cprel4$ make
```

Pass the `-DENABLE_EXAMPLES=Yes` option to cmake to build the examples.

The steps above can take a bit since gecode will be fetched and built.

## Tested systems

  - MacOS: Apple LLVM version 7.3.0 (clang-703.0.31)

    ```
    build-cprel4$ cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_COMPILER=c++   \
                        -DCMAKE_C_COMPILER=cc ../cprel4
    ```