/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "json-reader.hh"

#include <gecode/int.hh>
#include <gecode/set.hh>
#include <gecode/minimodel.hh>
#include <gecode/driver.hh>

using namespace std;
using namespace Gecode;

class DomAssign : public Script {
public:
  /**
   * Search options
   */
  enum {
    SEARCH_DFS, // Search for any solution(s)
    SEARCH_BAB  // Optimize
  };
  /**
   * Propagation options
   */
  enum {
    PROP_HARD,
    PROP_REIFIED
  };

private:
  /**
   * Data structures used to store the input
   */
  RelationMap* relations;
  OperationsList* operations;

private:
  /// Count of all the columns of the relations in the CSP
  int numColumns;
  /// Count of all the operations among relations in the CSP
  int numOperations;
  /**
   * @brief Array of columns (decision variable)
   *
   * Each column has a set of possible physical domains.
   */
  IntVarArray Domain;
  /**
   * @brief Matrix of boolean variables indicating a possible rename operation
   *     between two columns.
   *
   * mappings(i,j) is true if a rename between column @a i and @a j is required
   * by any operation and false otherwise.
   */
  SetVarArray Rename;
  /**
   * @brief Matrix to store if two columns @a i and @a j are related in the
   *     problem by any operation.
   */
  vector<vector<vector<int>>> operatedColumns;
  /**
   * @brief Total number of rename operations required by the instance.
   *
   * This variable is used for optimization.
   */
  IntVar totalMappings;

private:
  bool useReifiedVersion;

public:
  /////////
  // CSP //
  /////////
  DomAssign(const InstanceOptions& opt);
  /////////////////
  // Constraints //
  /////////////////
  /**
   * @brief Adds the required constraints to represent the set operation @a op.
   *
   * A set operation occurs between two relations LHS and RHS. the constraint
   * enforces that their domains are the same. That is, they have the same set
   * of attributes on the same physical domains.
   */
  void setOp(const Operation& op);
  /**
   * @brief Adds the required constraints to represent the join operation
   *     between two relations LHS and RHS.
   *
   * Every pair of columns participating in the join need the same physical
   * domain. Also, the columns in LHS and RHS that do not participate in the
   * join require pairwise different physical domains.
   */
  void joinOp(const Operation& op);
  /**
   * @brief Post a constraint ensuring that columns @a colA and @a colB must
   *     have different physical domains.
   */
  void ensureEqual(int opId, int colA, int colB);
  /**
   * @brief Post a constraint ensuring that columns @a colA and @a colB must
   *     have the same physical domain
   */
  void ensureDifferent(int opId, int colA, int colB);
  /**
   * @brief Represent the fact that columns @a colA and @a colB are related by
   *     an operation that will possibly require a mapping of one of them.
   */
  void possibleMapping(int opId, int colA, int colB);
  /**
   * @brief Post the constraint that all the columns in relation @a r need
   *     different physical domains.
   */
  void differentDomainsForColumnsOf(const Relation& r);
  /**
   * @brief Posts a constraint that the columns that are not related does not
   *     require a rename operation.
   *
   * This has to be called after posting the constraint for all the operation in
   * the input.
   */
  void finish(void);
  ///////////////////
  //  Optimization //
  ///////////////////
  virtual void constrain(const Space& _bestSoFar);
  ////////////
  // Search //
  ////////////
  DomAssign(bool share, DomAssign& s);
  virtual Space* copy(bool share);
  ////////////////////
  // Print solution //
  ////////////////////
  void print(std::ostream& os) const;
};
