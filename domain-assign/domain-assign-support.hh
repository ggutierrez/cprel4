/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <vector>
#include <string>
#include <cassert>
#include <iostream>
#include <set>
#include <algorithm>
#include <iterator>

class Relation {
private:
  std::vector<std::string> columns;
  std::vector<int> columnIds;
  std::string name;

private:
  static int nextColumnIndex;
  static int totalRelations;

public:
  Relation(void) : name("No name") {}
  Relation(const std::string& rname)
      : name(rname)
      , columns() {
    totalRelations++;
  }
  Relation(const Relation& r)
      : name(r.name)
      , columns(r.columns)
      , columnIds(r.columnIds) {}
  Relation& operator=(const Relation& rhs) {
    if (this != &rhs) {
      columns = rhs.columns;
      columnIds = rhs.columnIds;
      name = rhs.name;
    }
    return *this;
  }
  void addColumn(const std::string& cname) {
    columns.push_back(cname);
    columnIds.push_back(nextColumnIndex);
    nextColumnIndex++;
  }
  int getIdFromName(const std::string& name) const {
    for (int i = 0; i < columns.size(); i++) {
      if (columns[i] == name)
        return columnIds[i];
    }
    assert(false);
    return 0;
  }
  const std::string& getNameFromId(int c) const {
    for (int i = 0; i < columnIds.size(); i++) {
      if (columnIds[i] == c)
        return columns[i];
    }
    assert(false);
    return columns[0];
  }
  const std::vector<std::string>& getColumnNames(void) const { return columns; }

  const std::vector<int>& getColumnIds(void) const { return columnIds; }

  const std::string& getName(void) const { return name; }

  int getArity(void) const { return columnIds.size(); }

  static int getDeclaredRelations(void) { return totalRelations; }

  static int getTotalNumColumns(void) { return nextColumnIndex; }
};

int Relation::nextColumnIndex = 0;
int Relation::totalRelations = 0;

std::ostream& operator<<(std::ostream& os, const Relation& r) {
  os << "Relation: " << r.getName() << std::endl;
  const std::vector<std::string>& columns = r.getColumnNames();
  const std::vector<int>& columnIds = r.getColumnIds();
  for (int i = 0; i < columns.size(); i++) {
    os << "\t" << columns[i] << " \t" << columnIds[i] << std::endl;
  }
  return os;
}

class Operation {
private:
  std::string kind;
  std::string name;
  const Relation& lhs;
  const Relation& rhs;

  // Columns that are considered by the operation
  std::vector<int> lhsColumnsInMatch;
  std::vector<int> rhsColumnsInMatch;

  int operationId;

  static int nextOperationId;
  static int totalOperations;

public:
  Operation(const std::string& k, const std::string& n, const Relation& left,
            const Relation& right)
      : kind(k)
      , name(n)
      , lhs(left)
      , rhs(right)
      , operationId(nextOperationId) {
    totalOperations++;
    nextOperationId++;
  }
  Operation(const Operation& other)
      : kind(other.kind)
      , name(other.name)
      , lhs(other.lhs)
      , rhs(other.rhs)
      , operationId(other.operationId)
      , lhsColumnsInMatch(other.lhsColumnsInMatch)
      , rhsColumnsInMatch(other.rhsColumnsInMatch) {}
  void addColumnsInMatch(const std::string& lhsCol, const std::string& rhsCol) {
    lhsColumnsInMatch.push_back(lhs.getIdFromName(lhsCol));
    rhsColumnsInMatch.push_back(rhs.getIdFromName(rhsCol));
  }

  const std::string& getKind(void) const { return kind; }

  const Relation& getLHS(void) const { return lhs; }

  const Relation& getRHS(void) const { return rhs; }

  int getId(void) const { return operationId; }

  static int getTotalOperations(void) { return totalOperations; }

  const std::vector<int> getLeftColumnsInMatch(void) const {
    return lhsColumnsInMatch;
  }

  const std::vector<int> getRightColumnsInMatch(void) const {
    return rhsColumnsInMatch;
  }

private:
  static std::vector<int> notOperated(std::vector<int> total,
                                      std::vector<int> operated) {
    std::sort(begin(operated), end(operated));
    std::sort(begin(total), end(total));

    std::vector<int> result;
    std::set_difference(begin(total), end(total), begin(operated),
                        end(operated), std::inserter(result, begin(result)));
    return result;
  }

public:
  std::vector<int> getLeftColumnsNotInMatch(void) const {
    return notOperated(lhs.getColumnIds(), lhsColumnsInMatch);
  }
  std::vector<int> getRightColumnsNotInMatch(void) const {
    return notOperated(rhs.getColumnIds(), rhsColumnsInMatch);
  }
};

int Operation::nextOperationId = 0;
int Operation::totalOperations = 0;

std::ostream& operator<<(std::ostream& os, const Operation& op) {
  os << op.getKind() << std::endl;
  const auto& left = op.getLeftColumnsInMatch();
  const auto& right = op.getRightColumnsInMatch();

  for (int i = 0; i < left.size(); i++) {
    os << left[i] << ", " << right[i] << std::endl;
  }

  // Not operated
  const auto& noLeft = op.getLeftColumnsNotInMatch();

  for (int i = 0; i < noLeft.size(); i++) {
    os << " " << noLeft[i];
  }
  os << std::endl;

  const auto& noRight = op.getRightColumnsNotInMatch();

  for (int i = 0; i < noRight.size(); i++) {
    os << " " << noRight[i];
  }
  os << std::endl;

  return os;
}