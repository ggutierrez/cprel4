/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "domain-assign.hh"

/////////
// CSP //
/////////

DomAssign::DomAssign(const InstanceOptions& opt) {
  ///////////////////////////////////
  // Read instance from input file //
  ///////////////////////////////////
  relations = new RelationMap();
  operations = new OperationsList();
  useReifiedVersion = (opt.propagation() == DomAssign::PROP_REIFIED);
  readCSP(opt.instance(), *relations, *operations);
  ////////////////////////////
  // Initialize attributes  //
  ////////////////////////////
  numColumns = Relation::getTotalNumColumns();
  numOperations = Operation::getTotalOperations();

  Domain = IntVarArray(*this, numColumns, 1, numColumns);
  Rename = SetVarArray(*this, numColumns * numColumns, IntSet::empty, 0,
                       numOperations);

  operatedColumns =
      vector<vector<vector<int>>>(numColumns, vector<vector<int>>(numColumns));

  totalMappings = IntVar(*this, 0, numColumns * numColumns);
  /////////////////
  // Constraints //
  /////////////////

  // All the columns in one relation need different domains
  for (const auto& e : *relations) {
    differentDomainsForColumnsOf(e.second);
  }

  // Process the operations
  for (const Operation& op : *operations) {
    if (op.getKind() == "join") {
      joinOp(op);
    } else if (op.getKind() == "set") {
      // setOp(op);
    } else {
      std::cerr << "Unrecognized operation " << op << std::endl;
    }
  }

  finish();
  ///////////////
  // branching //
  ///////////////
  branch(*this, Domain, INT_VAR_NONE(), INT_VAL_MIN());
}

//////////////////////////
// Detailed constraints //
//////////////////////////

void DomAssign::setOp(const Operation& op) {
  const Relation& lhs = op.getLHS(), rhs = op.getRHS();

  // A set operation involves relations of the same arity
  assert(lhs.getArity() == rhs.getArity());

  // Also the correspondence is assumed to be in the order of declaration in
  // the domain of the relations

  for (int i = 0; i < lhs.getColumnIds().size(); i++) {
    int s = lhs.getColumnIds()[i];
    int t = rhs.getColumnIds()[i];
    // cout << "Columns " << r.getColumnNames()[i] << " and "
    //      << s.getColumnNames()[i]
    //      << " participate in a set operation and need the _same_ physical
    // domains"
    //      << endl;
    // ensureEqual(op.getId(),s,t);
    rel(*this, Domain[s] != Domain[t]);
  }
}

void DomAssign::joinOp(const Operation& op) {
  const Relation& r = op.getLHS(), s = op.getLHS();

  const std::vector<int>& sourceInMatch = op.getLeftColumnsInMatch(),
                          targetInMatch = op.getRightColumnsInMatch();

  assert(sourceInMatch.size() == targetInMatch.size());

  /**
   * Columns participating in the join must share their physical domains
   */
  for (int i = 0; i < sourceInMatch.size(); i++) {
    int scol = sourceInMatch[i];
    int tcol = targetInMatch[i];
    // cout << "Columns " << r.getNameFromId(scol) << " and column "
    //      << s.getNameFromId(tcol)
    //      << " participate in a join and must have the _same_ physical domain"
    //      << endl;
    ensureEqual(op.getId(), scol, tcol);
    possibleMapping(op.getId(), scol, tcol);
  }

  /**
   * Columns that do not participate in the join must have pairwise distinct
   * domains
   */
  for (const auto& i : op.getLeftColumnsNotInMatch()) {
    for (const auto& j : op.getRightColumnsNotInMatch()) {
      // cout << "Columns " << r.getNameFromId(i) << " and "
      //      << s.getNameFromId(j)
      //      << " participate in a join and need _different_ physical domains"
      //      << endl;
      ensureDifferent(op.getId(), i, j);
      possibleMapping(op.getId(), i, j);
    }
  }
}

void DomAssign::differentDomainsForColumnsOf(const Relation& r) {
  const auto& colIds = r.getColumnIds();
  const auto& colNames = r.getColumnNames();

  for (int i = 0; i < colIds.size(); i++) {
    for (int j = 0; j < colIds.size(); j++) {
      if (i != j && i < j) {
        // cout << "Columns " << colNames[i] << " and "
        //      << colNames[j]
        //      << " belong to the same relation and must be disjoint" << endl;
        rel(*this, Domain[i] != Domain[j]);
      }
    }
  }
}

void DomAssign::ensureEqual(int opId, int colA, int colB) {
  Matrix<SetVarArray> rename(Rename, numColumns, numColumns);
  IntSet s(opId, opId);
  if (useReifiedVersion) {
    BoolVar reify(*this, 0, 1);
    dom(*this, rename(colB, colA), SRT_SUP, s, reify);
    rel(*this, reify == (Domain[colA] != Domain[colB]));
  } else {
    rel(*this, Domain[colA] == Domain[colB]);
    // do not incur in a rename operation
    rel(*this, rename(colB, colA) || s);
  }
}

void DomAssign::ensureDifferent(int opId, int colA, int colB) {
  Matrix<SetVarArray> rename(Rename, numColumns, numColumns);
  IntSet s(opId, opId);
  if (useReifiedVersion) {
    BoolVar reify(*this, 0, 1);
    dom(*this, rename(colB, colA), SRT_SUP, s, reify);
    rel(*this, reify == (Domain[colA] == Domain[colB]));
  } else {
    rel(*this, Domain[colA] != Domain[colB]);
    // do not incur in a rename operation
    rel(*this, rename(colB, colA) || s);
  }
}

void DomAssign::possibleMapping(int opId, int colA, int colB) {
  vector<int>& operations = operatedColumns[colA][colB];
  operations.push_back(opId);
}

void DomAssign::finish(void) {

  Matrix<SetVarArray> rename(Rename, numColumns, numColumns);

  for (int i = 0; i < operatedColumns.size(); i++) {
    for (int j = 0; j < operatedColumns.size(); j++) {
      const vector<int>& operations = operatedColumns[i][j];
      int sz = operations.size();
      IntSet s(operations.data(), sz);
      rel(*this, rename(j, i) <= s);
    }
  }

  rel(*this, cardinality(setunion(Rename)) == totalMappings);
}

//////////////////
// Optimization //
//////////////////
void DomAssign::constrain(const Space& _bestSoFar) {
  const DomAssign& bestSoFar = static_cast<const DomAssign&>(_bestSoFar);
  int mappingsSoFar = bestSoFar.totalMappings.val();
  rel(*this, totalMappings < mappingsSoFar);
}

/////////////////////
// Solution output //
/////////////////////
void DomAssign::print(std::ostream& os) const {
  for (const auto& e : *relations) {
    const Relation& r = e.second;
    const vector<string>& columns = r.getColumnNames();
    const vector<int>& columnIds = r.getColumnIds();
    os << r.getName() << endl;
    for (int i = 0; i < columns.size(); i++) {
      os << "\t" << columns[i] << " \t" << columnIds[i] << " \t" << Domain[i]
         << std::endl;
    }
  }

  Matrix<SetVarArray> renames(Rename, numColumns, numColumns);

  for (int i = 0; i < numColumns; i++) {
    for (int j = 0; j < numColumns; j++) {
      os << " " << renames(i, j);
    }
    os << endl;
  }
  os << "Renames: " << totalMappings << endl;
}

////////////////////
// Search support //
////////////////////
DomAssign::DomAssign(bool share, DomAssign& s)
    : Script(share, s)
    , relations(s.relations)
    , operations(s.operations)
    , numColumns(s.numColumns)
    , numOperations(s.numOperations)
    , operatedColumns(s.operatedColumns) {
  Domain.update(*this, share, s.Domain);
  Rename.update(*this, share, s.Rename);
  totalMappings.update(*this, share, s.totalMappings);
}

Space* DomAssign::copy(bool share) { return new DomAssign(share, *this); }

int main(int argc, char* argv[]) {
  RelationMap relations;
  OperationsList operations;

  InstanceOptions opt("Domain Assignment solver");
  opt.instance("");
  opt.search(DomAssign::SEARCH_DFS, "dfs", "search for solution(s)");
  opt.search(DomAssign::SEARCH_BAB, "bab", "best solution search");
  opt.search(DomAssign::SEARCH_BAB);

  opt.propagation(DomAssign::PROP_HARD, "hard", "use hard constraints");
  opt.propagation(DomAssign::PROP_REIFIED, "reify", "use reified constraints");
  opt.propagation(DomAssign::PROP_REIFIED);

  opt.parse(argc, argv);

  std::string instance(opt.instance());
  if (instance != "") {
    if (opt.search() == DomAssign::SEARCH_BAB)
      Script::run<DomAssign, BAB, InstanceOptions>(opt);
    if (opt.search() == DomAssign::SEARCH_DFS)
      Script::run<DomAssign, DFS, InstanceOptions>(opt);
  }
  return 0;
}