/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "rel.hh"

using Gecode::ExecStatus;
using Gecode::PropCost;
using Gecode::ModEventDelta;
using Gecode::ES_OK;
using Gecode::ES_NOFIX;
using Gecode::ES_FIX;
using Gecode::AP_DISPOSE;

namespace MPG {
namespace Rel {

template <class View0, class View1, class View2>
class Intersection : public Propagator {
protected:
  View0 x;
  View1 y;
  View2 z;

public:
  /////////////
  // posting //
  /////////////
  Intersection(Space& home, View0 x0, View1 y0, View2 z0)
      : Propagator(home)
      , x(x0)
      , y(y0)
      , z(z0) {

    assert(setCompatible(x0.schema(), y0.schema(), z0.schema()));
    home.notice(*this, AP_DISPOSE);
    x.subscribe(home, *this, PC_REL_ANY);
    y.subscribe(home, *this, PC_REL_ANY);
    z.subscribe(home, *this, PC_REL_ANY);
  }
  static ExecStatus post(Space& home, View0 x0, View1 y0, View2 z0) {
    (void)new (home) Intersection(home, x0, y0, z0);
    getCPRelStats().interPropPosts++;
    return ES_OK;
  }
  //////////////
  // disposal //
  //////////////
  virtual size_t dispose(Space& home) {
    home.ignore(*this, AP_DISPOSE);
    x.cancel(home, *this, PC_REL_ANY);
    y.cancel(home, *this, PC_REL_ANY);
    z.cancel(home, *this, PC_REL_ANY);
    (void)Propagator::dispose(home);
    return sizeof(*this);
  }
  /////////////
  // copying //
  /////////////
  Intersection(Space& home, bool share, Intersection& p)
      : Propagator(home, share, p)
      , x(p.x)
      , y(p.y)
      , z(p.z) {
    x.update(home, share, p.x);
    y.update(home, share, p.y);
    z.update(home, share, p.z);
  }
  virtual Propagator* copy(Space& home, bool share) {
    return new (home) Intersection(home, share, *this);
  }
  //////////////////////
  // cost computation //
  //////////////////////
  virtual PropCost cost(const Space&, const ModEventDelta&) const {
    return PropCost::binary(PropCost::LO);
  }
  /////////////////
  // propagation //
  /////////////////
  virtual ExecStatus propagate(Space& home, const ModEventDelta&) {
    LOG_OPERATION_NARGS("Prop::intersect");
    getCPRelStats().interPropExecutions++;

    // z \subseteq (\glb{x} \cap \glb{y})
    GECODE_ME_CHECK(z.include(home, x.glb() && y.glb()));

    // (\lub{x} \cap \lub{y}) \subseteq z
    GECODE_ME_CHECK(z.exclude(home, !(x.lub() && y.lub())));

    // x \subseteq \glb{z}
    GECODE_ME_CHECK(x.include(home, z.glb()));

    // y \subseteq \glb{z}
    GECODE_ME_CHECK(y.include(home, z.glb()));

    // (\glb{x} \setdiff \lub{z}) \subseteq y
    GECODE_ME_CHECK(y.exclude(home, (x.glb() - z.lub())));

    // (\glb{y} \setdiff \lub{z}) \subseteq x
    GECODE_ME_CHECK(x.exclude(home, (y.glb() - z.lub())));

    if (x.assigned() && y.assigned() && z.assigned())
      return home.ES_SUBSUMED(*this);

    return ES_NOFIX;
  }
};
}
}
