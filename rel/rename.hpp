/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "rel.hh"

using Gecode::ExecStatus;
using Gecode::PropCost;
using Gecode::ModEventDelta;
using Gecode::ES_OK;
using Gecode::ES_NOFIX;
using Gecode::ES_FIX;
using Gecode::ES_FAILED;
using Gecode::AP_DISPOSE;

using CuddAbstraction::Schema;
using CuddAbstraction::AttributeMap;

namespace MPG {
namespace Rel {

/**
 * @brief Propagator for the constraint x = y
 *
 * This propagator works when @a x and @y are relation decision variables on a
 * different schema. The correspondence between the attributes of @a x and the
 * attributes of @a y is provided by the attribute map @a m.
 *
 */
template <class View0, class View1>
class Rename : public Propagator {
protected:
  View0 x;                 ///> LHS variable
  const AttributeMap m;    ///> Attribute correspondence between @a x and @a y
  const AttributeMap mInv; ///> Attribute correspondence between @a y and @a x
  View1 y;                 ///> RHS variable

public:
  /////////////
  // posting //
  /////////////
  Rename(Space& home, View0 y0, const AttributeMap& c, View1 y1)
      : Propagator(home)
      , x(y0)
      , m(c)
      , mInv(c.inverse())
      , y(y1) {
    // TODO: Should this code go on the post function?
    assert(m.mappingBetween(y0.schema(), y1.schema()));

    // If the map represents the identity this constraint is equivalent to the
    // equality constraint
    assert(!m.isIdentity());

    /*
      As this class has an AttributeMap object that needs to be destructed (for
      references counting purposes) we need to inform the Gecode kernel that the
      dispose method has to be called.
     */
    home.notice(*this, AP_DISPOSE);
    x.subscribe(home, *this, PC_REL_ANY);
    y.subscribe(home, *this, PC_REL_ANY);
  }
  static ExecStatus post(Space& home, View0 x, const AttributeMap& c, View1 y) {
    (void)new (home) Rename(home, x, c, y);
    getCPRelStats().renamePropPosts++;
    return ES_OK;
  }

public:
  //////////////
  // disposal //
  //////////////
  virtual size_t dispose(Space& home) {
    home.ignore(*this, AP_DISPOSE);
    x.cancel(home, *this, PC_REL_ANY);
    y.cancel(home, *this, PC_REL_ANY);
    m.~AttributeMap();
    mInv.~AttributeMap();
    (void)Propagator::dispose(home);
    return sizeof(*this);
  }

public:
  /////////////
  // copying //
  /////////////
  Rename(Space& home, bool share, Rename& p)
      : Propagator(home, share, p)
      , m(p.m)
      , mInv(p.mInv) {
    x.update(home, share, p.x);
    y.update(home, share, p.y);
  }
  virtual Propagator* copy(Space& home, bool share) {
    return new (home) Rename(home, share, *this);
  }

public:
  //////////////////////
  // cost computation //
  //////////////////////
  virtual PropCost cost(const Space&, const ModEventDelta&) const {
    return PropCost::crazy(PropCost::HI, 42);
  }
  /////////////////
  // propagation //
  /////////////////
  /**
   * @brief Returns the relation r renamed using the map @a m
   */
  static Relation renameTo(const Relation& r, const AttributeMap& map,
                           const Schema& s) {
    auto& relHome = getRelationSpace();
    assert(map.mappingBetween(r.schema(), s));
    return r.withSchema(relHome, map, s);
  }
  Relation withDomainOfX(const Relation& r) const {
    assert(r.schema() == y.glb().schema());
    return renameTo(r, mInv, x.schema());
  }

  Relation withDomainOfY(const Relation& r) const {
    assert(r.schema() == x.glb().schema());
    return renameTo(r, m, y.schema());
  }

  virtual ExecStatus propagate(Space& home, const ModEventDelta&) {
    LOG_OPERATION_NARGS("Prop::rename");
    getCPRelStats().renamePropExecutions++;

    {
      GECODE_ME_CHECK(x.include(home, withDomainOfX(y.glb())));

      Relation r_domOfX = x.lub().intersectWith(withDomainOfX(y.lub()));
      GECODE_ME_CHECK(x.exclude(home, r_domOfX.complement()));
    }

    {
      GECODE_ME_CHECK(y.include(home, withDomainOfY(x.glb())));

      Relation r_domOfY = y.lub().intersectWith(withDomainOfY(x.lub()));
      GECODE_ME_CHECK(y.exclude(home, r_domOfY.complement()));
    }

    if (x.assigned() && y.assigned())
      return home.ES_SUBSUMED(*this);

    return ES_FIX;
  }
};
}
}
