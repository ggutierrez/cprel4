/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

using Gecode::ConstView;
using Gecode::DerivedView;
using Gecode::ModEventDelta;
using Gecode::VarImpView;

////////////////////
// Relation view  //
////////////////////
namespace MPG {
namespace Rel {

class RelView : public VarImpView<RelVar> {
protected:
  using VarImpView<RelVar>::x;

public:
  RelView(void) {}
  RelView(const RelVar& y)
      : VarImpView<RelVar>(y.varimp()) {}
  RelView(RelVarImp* y)
      : VarImpView<RelVar>(y) {}

public:
  // access operations
  const Relation& glb(void) const { return x->glb(); }
  const Relation& lub(void) const { return x->lub(); }
  const Schema& schema(void) const { return x->glb().schema(); }

public:
  // modification operations
  ModEvent include(Space& home, const Relation& r) {
    return x->include(home, r);
  }
  ModEvent exclude(Space& home, const Relation& r) {
    return x->exclude(home, r);
  }

public:
  // delta information
  bool glbChanged(const Delta& d) const { return RelVarImp::glbChanged(d); }
  bool lubChanged(const Delta& d) const { return RelVarImp::lubChanged(d); }
};

// Controls the printing of relations
static int const printHTML = std::ios_base::xalloc();

inline std::ostream& setHTMLPrinting(std::ostream& os) {
  os.iword(printHTML) = 1;
  return os;
}

inline std::ostream& setRawPrinting(std::ostream& os) {
  os.iword(printHTML) = 0;
  return os;
}

template <class Char, class Traits>
std::basic_ostream<Char, Traits>&
operator<<(std::basic_ostream<Char, Traits>& os, const RelView& x) {
  std::basic_ostringstream<Char, Traits> s;
  s.copyfmt(os);
  s.width(0);
  using namespace CuddAbstraction;
  os << "GLB:\n";
  Formatter fmtGlb(x.glb(), os);
  fmtGlb.print(getRelationSpace());
  if (!x.assigned()) {
    Relation unk = x.lub() - x.glb();
    os << "UNK:\n";
    Formatter fmtUnk(unk, os);
    fmtUnk.print(getRelationSpace());
  }
  return os << s.str();
}

template <class Char, class Traits>
void print(std::basic_ostream<Char, Traits>& os,
           CuddAbstraction::DomainGroup& dg, const RelView& x) {
  using namespace CuddAbstraction;
  os << "GLB:\n";
  Formatter fmtGlb(x.glb(), os, &dg);
  fmtGlb.print(getRelationSpace());
  if (!x.assigned()) {
    os << "UNK:\n";
    Formatter fmtUnk(x.lub() - x.glb(), os, &dg);
    fmtUnk.print(getRelationSpace());
  }
}
}
}

////////////////////////////
// Constant relation view //
////////////////////////////
namespace MPG {
namespace Rel {

class ConstRelView : public ConstView<RelView> {
protected:
  Relation x;

public:
  ConstRelView(void) = delete; //: x(0) {}
  ConstRelView(const Relation& r)
      : x(r) {}

  const Relation& glb(void) const { return x; }
  const Relation& lub(void) const { return x; }
  const Schema& schema(void) const { return x.schema(); }
  ModEvent include(Space& home, const Relation& r) {
    return (r.isSubsetOf(x)) ? ME_REL_NONE : ME_REL_FAILED;
  }
  ModEvent exclude(Space& home, const Relation& r) {
    return (r.isDisjointWith(x)) ? ME_REL_NONE : ME_REL_FAILED;
  }
  // delta information
  bool glbChanged(const Delta& d) const {
    GECODE_NEVER;
    return false;
  }
  bool lubChanged(const Delta& d) const {
    GECODE_NEVER;
    return false;
  }
  // update during cloning
  void update(Space& home, bool share, ConstRelView& y) {
    ConstView<RelView>::update(home, share, y);
    x = y.x;
  }
};
// view tests
inline bool same(const ConstRelView& x, const ConstRelView& y) {
  return x.glb() == y.glb();
}
inline bool before(const ConstRelView& x, const ConstRelView& y) {
  if (x.glb().schema() == y.glb().schema())
    return x.glb().cardinality() < y.glb().cardinality();
  return x.glb().isSubsetOf(y.glb());
}

template <class Char, class Traits>
std::basic_ostream<Char, Traits>&
operator<<(std::basic_ostream<Char, Traits>& os, const ConstRelView& x) {
  return os << x.glb();
}
}
}

/////////////////////
// Complement view //
/////////////////////
namespace MPG {
namespace Rel {

class ComplementView : public DerivedView<RelView> {
protected:
  using DerivedView<RelView>::x;
  ////////////////////////////////////////////////////
  // Modification events and propagation conditions //
  ////////////////////////////////////////////////////
  static ModEvent complementme(ModEvent me) {
    switch (me) {
    case ME_REL_GLB:
      return ME_REL_LUB;
    case ME_REL_LUB:
      return ME_REL_GLB;
    default:
      return me;
    }
  }
  static PropCond complementpc(PropCond pc) {
    switch (pc) {
    case PC_REL_GLB:
      return PC_REL_LUB;
    case PC_REL_LUB:
      return PC_REL_GLB;
    default:
      return pc;
    }
  }

public:
  ComplementView(void) {}
  explicit ComplementView(const RelView& y)
      : DerivedView<RelView>(y) {}

public:
  ///////////////////////
  // Access operations //
  ///////////////////////
  Relation glb(void) const { return x.lub().complement(); }
  Relation lub(void) const { return x.glb().complement(); }
  const Schema& schema(void) const { return x.glb().schema(); }

public:
  /////////////////////////////
  // Modification operations //
  /////////////////////////////
  ModEvent include(Space& home, const Relation& r) {
    return complementme(x.exclude(home, r));
  }
  ModEvent exclude(Space& home, const Relation& r) {
    return complementme(x.include(home, r));
  }

public:
  ////////////////////////
  // Support operations //
  ////////////////////////
  static void schedule(Space& home, Propagator& p, ModEvent me) {
    return RelView::schedule(home, p, complementme(me));
  }
  static ModEvent me(const ModEventDelta& med) {
    return complementme(RelView::me(med));
  }
  static ModEventDelta med(ModEvent me) {
    return RelView::med(complementme(me));
  }

public:
  ///////////////////
  // Subscriptions //
  ///////////////////
  void subscribe(Space& home, Propagator& p, PropCond pc,
                 bool schedule = true) {
    x.subscribe(home, p, complementpc(pc), schedule);
  }
  void subscribe(Space& home, Advisor& a) { x.subscribe(home, a); }
  void cancel(Space& home, Propagator& p, PropCond pc) {
    x.cancel(home, p, complementpc(pc));
  }
  void cancel(Space& home, Advisor& a) { x.cancel(home, a); }

public:
  ///////////////////////
  // Delta information //
  ///////////////////////
  static ModEvent modevent(const Delta& d) {
    return complementme(RelView::modevent(d));
  }
  bool glbChanged(const Delta& d) const { return x.lubChanged(d); }
  bool lubChanged(const Delta& d) const { return x.glbChanged(d); }
};

template <class Char, class Traits>
std::basic_ostream<Char, Traits>&
operator<<(std::basic_ostream<Char, Traits>& os, const ComplementView& x) {
  std::basic_ostringstream<Char, Traits> s;
  s.copyfmt(os);
  s.width(0);

  using CuddAbstraction::print;
  if (x.assigned())
    os << "val:\n";
  else
    os << "glb:\n";

  print(getRelationSpace(), x.glb(), os);

  if (!x.assigned()) {
    os << "unknown:\n";
    print(getRelationSpace(), x.lub() - x.glb(), os);
  }
  return os << s.str();
}
}
}
