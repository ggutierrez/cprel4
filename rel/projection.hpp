/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "rel.hh"
#include "format.h"

#include <memory>

using std::shared_ptr;
using std::make_shared;

using Gecode::ExecStatus;
using Gecode::PropCost;
using Gecode::ModEventDelta;
using Gecode::ES_OK;
using Gecode::ES_NOFIX;
using Gecode::ES_FIX;
using Gecode::ES_FAILED;
using Gecode::AP_DISPOSE;

using CuddAbstraction::Schema;
using CuddAbstraction::Attribute;
using CuddAbstraction::UniqueAbstractor;

namespace MPG {
namespace Rel {

enum class UniqueAbstractBehavior {
  /**
   * Tries to use the fast version of unique abstraction whenever possible.
   */
  TRY_FAST,
  /**
   * Uses the mathematical definition of uniqueness
   */
  MATH_DEF
};

template <class View0, class View1>
class Projection : public Propagator {
protected:
  View0 x;
  const Schema a;
  View1 y;
  /// Stores: schema(x) - schema(y)
  Schema xMinusY;
  /// Unique abstractor used by the propagator
  shared_ptr<UniqueAbstractor> abstractor;
  /// Can use fast unique abstraction
  const bool useFastAbstract;
  /// Behavior to compute unique abstraction
  UniqueAbstractBehavior uniqueBehavior;

  /////////////
  // posting //
  /////////////
private:
  shared_ptr<UniqueAbstractor> initAbstractor(UniqueAbstractBehavior bhv) {
    if (bhv == UniqueAbstractBehavior::TRY_FAST)
      return nullptr;
    else {                                  // UniqueAbstractBehavior::MATH_DEF
      return make_shared<UniqueAbstractor>( //
          x.lub().createUniqueAbstractor(getRelationSpace(),
                                         a.asColumnDomains()));
    }
  }

public:
  Projection(Space& home, View0 y0, const Schema& a0, View1 y1,
             bool useFastAbstract, UniqueAbstractBehavior bhv)
      : Propagator(home)
      , x(y0)
      , a(a0)
      , y(y1)
      , xMinusY(x.glb().schema().differenceWith(getRelationSpace(),
                                                y.glb().schema()))
      , abstractor(initAbstractor(bhv))
      , useFastAbstract(useFastAbstract)
      , uniqueBehavior(bhv) {

    // abstractor->debugInfo();

    // TODO: throw exception if a is not y.domain()
    home.notice(*this, AP_DISPOSE);
    x.subscribe(home, *this, PC_REL_ANY);
    y.subscribe(home, *this, PC_REL_ANY);
  }
  /**
   * @brief Output debug information regarding the propogator posting.
   */
  static void debugInfo(View0 x, const Schema& a0, View1 y, bool fastAbstract) {
    // fmt::print_colored(fmt::RED, "Fast? {}\n", fastAbstract);
    fmt::print("{{ \"Projection post\":{{\n");
    fmt::print("\"Projected schema\":\n{},\n",
               x.glb().schema().debugInfo(true));
    fmt::print("\"Relation size\":\n{},\n", x.lub().cardinality());
    fmt::print("\"Full?\":\n{},\n", x.lub().isFull());
    fmt::print("\"Project on\":\n{},\n", a0.debugInfo());
    fmt::print("\"Fast abstract\": {} }} }},\n", fastAbstract);
  }
  static ExecStatus
  post(Space& home, View0 x, const Schema& a0, View1 y,
       UniqueAbstractBehavior bhv = UniqueAbstractBehavior::MATH_DEF) {

    bool fastAbstract =
        x.lub().canUseFastUniqueWithRespectTo(getRelationSpace(), a0);
    // debugInfo(x, a0, y, fastAbstract);
    (void)new (home) Projection(home, x, a0, y, fastAbstract, bhv);
    getCPRelStats().projPropPosts++;
    return ES_OK;
  }
  //////////////
  // disposal //
  //////////////
  virtual size_t dispose(Space& home) {
    home.ignore(*this, AP_DISPOSE);
    x.cancel(home, *this, PC_REL_ANY);
    y.cancel(home, *this, PC_REL_ANY);
    a.~Schema();
    xMinusY.~Schema();
    abstractor.~shared_ptr<UniqueAbstractor>();
    (void)Propagator::dispose(home);
    return sizeof(*this);
  }
  /////////////
  // copying //
  /////////////
  Projection(Space& home, bool share, Projection& p)
      : Propagator(home, share, p)
      , a(p.a)
      , xMinusY(p.xMinusY)
      , abstractor(p.abstractor)
      , useFastAbstract(p.useFastAbstract)
      , uniqueBehavior(p.uniqueBehavior) {

    x.update(home, share, p.x);
    y.update(home, share, p.y);
  }
  virtual Propagator* copy(Space& home, bool share) {
    return new (home) Projection(home, share, *this);
  }
  //////////////////////
  // cost computation //
  //////////////////////
  virtual PropCost cost(const Space&, const ModEventDelta& med) const {
    ModEvent meX = View0::me(med);
    ModEvent meY = View1::me(med);

    if (testRelEventUB(meX) || testRelEventLB(meY)) {
      // This case for sure will require unique quantification to be executed
      // and hence the cost of the propagator is very high. 10 has no
      // explanation but I need to really schedule the projection constraint as
      // few times as possible. I could have used a binary cost but then the
      // constraint would be scheduled before join and join is cheaper.
      //
      // A much better approach is to actually decide the cost in terms of the
      // sizes of the BDD representations. However computing the sizes take time
      // and I don't want that overhead.
      return PropCost::crazy(PropCost::HI, 10);
    } else {
      // In any other case the most expensive operation is projection that
      // requires existential abstraction.
      return PropCost::binary(PropCost::HI);
    }
  }
  /////////////////
  // propagation //
  /////////////////
  /**
   * @brief Narrows lub(y)
   *
   * lub(y) <= lub(y) \cap \project{a}{lub(x)}
   *
   * Modifies: lub(y)
   * Uses:     lub(y), lub(x)
   * Most costly operations: projection and intersection
   */
  ModEvent narrowYLUB(Space& home) {
    Relation ubi = y.lub() && (x.lub() | a);
    return y.exclude(home, !ubi);
  }
  /**
   * @brief Narrows lub(x)
   *
   * The maximum relation which is possible in x is the one that can be derived
   * by using y and assuming the missing columns have all their possible values.
   *
   * lub(x) <= lub(x) \cap (lub(y) * (\attributes{x} - \attributes{y}))
   *
   * Modifies: lub(x)
   * Uses:     lub(x) lub(y)
   * Most costly operations: intersection
   */
  ModEvent narrowXLUB(Space& home) {
    Relation MaxPossibleSupportFromY = y.lub() * xMinusY;
    return x.exclude(home, !(x.lub() && MaxPossibleSupportFromY));
  }
  /**
   * @brief @brief Narrows glb(y)
   *
   * glb(y) <= glb(y) \cup \project{a}{glb(x)}
   *
   * Modifies: glb(y)
   * Uses:     glb(y) glb(x)
   * Most costly operations: union and projection
   */
  ModEvent narrowYGLB(Space& home) {
    Relation lbu = y.glb() || (x.glb() | a);
    return y.include(home, lbu);
  }
  /**
   * @brief Narrows glb(x).
   *
   * This is the naive approach to the narrowing of glb(x). It always call
   * unique quantification.
   *
   * To support glb(y):
   * 1- Compute the relation needed to support glb(y) from x.
   *
   * Modifies: glb(x)
   * Uses:     glb(x) glb(y) lub(x)
   * Most costly operation: unique quantification
   */
  ModEvent narrowXGLB(Space& home) {
    Relation imminentInX(getRelationSpace(), x.lub().schema());
    if (uniqueBehavior == UniqueAbstractBehavior::TRY_FAST) {
      imminentInX = x.lub().uniqueWithRespectTo(
          getRelationSpace(), a.asColumnDomains(), useFastAbstract);
    } else {
      imminentInX =
          x.lub().uniqueWithRespectTo(getRelationSpace(), *abstractor);
    }
    Relation required = y.glb() * xMinusY;
    Relation lbx = x.glb() || (required && imminentInX);
    return x.include(home, lbx);
  }

  /**
   * @brief Propagates changes to lub(y)
   *
   * Propagates the changes to lub(y) if @a cond.
   *
   * @param med Modification delta
   * @param force whether propagation must be forced independent from the delta
   * @return ME_REL_NONE if the propagation was not required or the
   *     corresponding result.
   */
  ModEvent propagateYLUB(Space& home, bool cond) {
    return cond ? narrowYLUB(home) : ME_REL_NONE;
  }

  ModEvent propagateXLUB(Space& home, bool cond) {
    return cond ? narrowXLUB(home) : ME_REL_NONE;
  }

  ModEvent propagateYGLB(Space& home, bool cond) {
    return cond ? narrowYGLB(home) : ME_REL_NONE;
  }

  /**
   * @brief Propagates the changes to glb(x)
   *
   * - glbYlubXMod0
   * - @a force indicates if the actual narrowing must take place or not
   * - @a imminentInX is the subrelation of @a x that is unique with respect to
   *      the set of attributes @a a. As an optimization this value is computed
   *      only when required. That is, when lub(x) have changed since last time
   *      it was computed.
   */
  ModEvent propagateXGLB(Space& home, bool cond, Relation& imminentInX,
                         bool yLUBModified) {
    return cond ? narrowXGLB(home, imminentInX, yLUBModified) : ME_REL_NONE;
  }

  ModEvent propagateXGLB(Space& home, bool cond) {
    return cond ? narrowXGLB(home) : ME_REL_NONE;
  }

  /**
   * @brief Propagation
   *
   * This version does not care about idempotency or any other property. It just
   * applies the filtering rules and returns.
   *
   * Note: so far it seems this is the strategy  reporting the best performance.
   */
  virtual ExecStatus propagate(Space& home, const ModEventDelta& med) {
    getCPRelStats().projPropExecutions++;
    // std::cout << "Projection propagate\n";
    GECODE_ME_CHECK(narrowYLUB(home));
    GECODE_ME_CHECK(narrowXLUB(home));
    GECODE_ME_CHECK(narrowYGLB(home));
    GECODE_ME_CHECK(narrowXGLB(home));

    // Fix point
    if (x.assigned()) {
      assert(y.assigned());
      return home.ES_SUBSUMED(*this);
    }
    return ES_NOFIX;
  }

  /**
   * @brief Propagation
   *
   * This version implements an idempotent propagator. It applies every
   * propagation rule until no further change is obtained.
   *
   * Note: this version is slow when compared to the non-idempotent one.
   */
  // ExecStatus propagateIDEM(Space& home, const ModEventDelta& med) {
  //   bool modified = false;
  //   do {
  //     modified = false;
  //     GECODE_ME_CHECK_MODIFIED(modified, narrowYLUB(home));
  //     GECODE_ME_CHECK_MODIFIED(modified, narrowXLUB(home));
  //     GECODE_ME_CHECK_MODIFIED(modified, narrowYGLB(home));
  //     GECODE_ME_CHECK_MODIFIED(modified, narrowXGLB(home));
  //   } while (modified);

  //   // Fix point
  //   if (x.assigned()) {
  //     assert(y.assigned());
  //     return home.ES_SUBSUMED(*this);
  //   }
  //   return ES_FIX;
  // }
};
}
}
