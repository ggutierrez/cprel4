/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "rel.hh"
#include <vector>
#include <algorithm>

using Gecode::ExecStatus;
using Gecode::PropCost;
using Gecode::ModEventDelta;
using Gecode::ES_OK;
using Gecode::ES_NOFIX;
using Gecode::ES_FIX;
using Gecode::ES_FAILED;

using Gecode::Int::PC_INT_DOM;

using CuddAbstraction::Schema;

namespace MPG {
namespace Rel {
/**
 * @brief Channel constraint between a relation variable and an integer
 *     variable representing its cardinality.
 */
template <class RView, class IView>
class Cardinality : public Propagator {
protected:
  RView x; ///< Relation variable
  IView y; ///< Integer variable (cardinality)
public:
  /////////////
  // posting //
  /////////////
  Cardinality(Space& home, RView x0, IView x1)
      : Propagator(home)
      , x(x0)
      , y(x1) {
    // TODO: appropriate subscriptions
    x.subscribe(home, *this, PC_REL_ANY);
    y.subscribe(home, *this, PC_INT_DOM);
  }
  static ExecStatus post(Space& home, RView x0, IView y0) {
    (void)new (home) Cardinality(home, x0, y0);
    getCPRelStats().cardPropPosts++;
    return ES_OK;
  }
  //////////////
  // disposal //
  //////////////
  virtual size_t dispose(Space& home) {
    // TODO: appropriate disposal
    x.cancel(home, *this, PC_REL_ANY);
    y.cancel(home, *this, PC_INT_DOM);
    (void)Propagator::dispose(home);
    return sizeof(*this);
  }
  /////////////
  // copying //
  /////////////
  Cardinality(Space& home, bool share, Cardinality& p)
      : Propagator(home, share, p) {
    x.update(home, share, p.x);
    y.update(home, share, p.y);
  }
  virtual Propagator* copy(Space& home, bool share) {
    return new (home) Cardinality(home, share, *this);
  }
  //////////////////////
  // cost computation //
  //////////////////////
  virtual PropCost cost(const Space&, const ModEventDelta&) const {
    return PropCost::binary(PropCost::LO);
  }
  /////////////////
  // propagation //
  /////////////////
  virtual ExecStatus propagate(Space& home, const ModEventDelta&) {
    getCPRelStats().cardPropExecutions++;

    // Propagation based on the information of the relation variable.
    {
      if (x.lub().cardinality() < y.min())
        return ES_FAILED;

      if (x.glb().cardinality() > y.max())
        return ES_FAILED;
    }

    // Propagation based on the information of the cardinality
    {
      GECODE_ME_CHECK(
          y.gq(home, static_cast<long long int>(x.glb().cardinality())));
      GECODE_ME_CHECK(
          y.lq(home, static_cast<long long int>(x.lub().cardinality())));
    }
    // TODO: Check fix point reasoning
    if (x.assigned() && y.assigned())
      return home.ES_SUBSUMED(*this);

    return ES_FIX;
  }
};
}
}
