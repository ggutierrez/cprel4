/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <memory>

#include "rel.hh"
#include "format.h"

namespace MPG {
namespace Rel {
namespace internal {
/**
 * @brief BDD manager used by all variable implementations.
 *
 * This manager is initialized at the early stages of the solver.
 */
std::unique_ptr<CuddAbstraction::SpaceManager> relManager;
}

void initializeRelationSpace(bool statsOnDestruction,
                             unsigned int slotsInUniqueTable,
                             unsigned int slotsInCache, unsigned long maxMemory,
                             unsigned int cacheMinEff,
                             unsigned int maxSlotsInCache,
                             bool dynamicReorder) {
  // Prevent multiple initializations attempts.
  if (internal::relManager) {
    std::cerr << "Attempt to reinitialize the relation manager\n";
    std::abort();
  }

  internal::relManager = std::unique_ptr<CuddAbstraction::SpaceManager>(
      new CuddAbstraction::SpaceManager(statsOnDestruction, slotsInUniqueTable,
                                        slotsInCache, maxMemory, cacheMinEff,
                                        maxSlotsInCache, dynamicReorder));

  /*
    std::cout << "BDD Manager created:\n"
              << "[Initial unique table slots]: " << slotsInUniqueTable << '\n'
              << "[Initial cache size]: " << slotsInCache << '\n'
              << "[Cache hard limit]: "
              << internal::relManager->ReadMaxCacheHard() << '\n'
              << "[Cache minimum hit]: " << internal::relManager->ReadMinHit()
              << '\n' << "[Maximum memory limit]: " << (maxMemory / MemSize::MB)
              << " MB.\n";
              */
}

SpaceManager& getRelationSpace(void) {
  if (!internal::relManager) {
    fmt::print(stderr, "Default initialized relation space\n");
    initializeRelationSpace();
  }
  return *internal::relManager;
}

namespace internal {
/**
 * @brief CPRel statistics
 */
std::unique_ptr<CPRelStats> cprelStats =
    std::unique_ptr<CPRelStats>(new CPRelStats());
}
/// Returns the statiscis object
CPRelStats& getCPRelStats(void) { return *internal::cprelStats; }
/////////////////////////////
// Variable implementation //
/////////////////////////////
RelVarImp::RelVarImp(Space& home, const Relation& glb, const Relation& lub)
    : RelVarImpBase(home)
    , glb_(glb)
    , lub_(lub) {}

RelVarImp::RelVarImp(Space& home, const Schema& sch)
    : RelVarImpBase(home)
    , glb_(empty(sch))
    , lub_(full(sch)) {}
}
}

Gecode::VarImpDisposer<MPG::Rel::RelVarImp> relDisposer;