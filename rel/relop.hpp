/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

namespace MPG {
namespace Rel {
/**
 * @brief Operator overloading for relation intersection
 *
 * @param r LHS relation
 * @param s RHS relation
 */
inline Relation operator&&(const Relation& r, const Relation& s) {
  return r.intersectWith(s);
}
/**
 * @brief Operator overloading for relation union
 *
 * @param r LHS relation
 * @param s RHS relation
 */
inline Relation operator||(const Relation& r, const Relation& s) {
  return r.unionWith(s);
}
/**
 * @brief Operator overloading for relation complement
 *
 * @param r Relation to be complemented
 */
inline Relation operator!(const Relation & r) { return r.complement(); }

/**
 * @brief Operator overloading for relation difference
 * @details Computes @a r - @a s
 *
 * @param r LHS relation
 * @param s RHS relation
 */
inline Relation operator-(const Relation& r, const Relation& s) {
  return r.differenceWith(s);
}

/**
 * @brief Operator overloading for relation projection
 * @details Computes the projection of @a r on the schema @a sch
 */
inline Relation operator|(const Relation& r, const Schema& sch) {
  return r.projectTo(getRelationSpace(), sch);
}
/**
 * @brief Operator overloading for cross product of relations
 * @details Computes @a r x @a F(sch), where F(sch) represents the full relation
 *     with schema @a sch.
 */
inline Relation operator*(const Relation& r, const Schema& sch) {
  return r.crossWith(getRelationSpace(), sch);
}
/**
 * @brief Operator overloading to compute the join of relations @a r and @a s
 */
inline Relation operator*(const Relation& r, const Relation& s) {
  return r.joinWith(getRelationSpace(), s);
}
}
/**
 * @brief Creates a new schema with the list of attributes @a a
 */
inline Schema
schema(const std::initializer_list<CuddAbstraction::Attribute>& a) {
  return Schema(Rel::getRelationSpace(), a);
}
/**
 * @brief Creates an empty relation on schema @a sch
 */
inline Relation empty(const Schema& sch) {
  return Relation::createEmpty(Rel::getRelationSpace(), sch);
}
/**
 * @brief Creates a full relation on schema @a sch
 */
inline Relation full(const Schema& sch) {
  return Relation::createFull(Rel::getRelationSpace(), sch);
}
}