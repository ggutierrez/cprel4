/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "rel.hh"

namespace MPG {
using Rel::RelView;
using Rel::ComplementView;
using Rel::ConstRelView;

using Gecode::Set::SetView;
using Gecode::Int::IntView;

////////////////////////////
// Equality post function //
////////////////////////////
void equal(Space& home, RelVar x, RelVar y) {
  if (home.failed())
    return;

  if (!setCompatible(x.schema(), y.schema())) {
    throw Rel::SetIncompatible("equal");
  }

  RelView vx(x), vy(y);
  if (Rel::Equality<RelView, RelView>::post(home, vx, vy) != ES_OK)
    home.fail();
}

void equal(Space& home, RelVar x, SetVar y) {
  if (home.failed())
    return;

  if (x.schema().arity() != 1) {
    throw Rel::SetIncompatible("equal");
  }
  RelView vx(x);
  SetView vy(y);
  if (Rel::SetEquality<RelView, SetView>::post(home, vx, vy) != ES_OK)
    home.fail();
}

//////////////////////////////
// Complement post function //
//////////////////////////////
void complement(Space& home, RelVar x, RelVar y) {
  if (home.failed())
    return;

  if (!setCompatible(x.schema(), y.schema())) {
    throw Rel::SetIncompatible("complement");
  }

  RelView vx(x), vy(y);
  ComplementView cvy(vy);

  if (Rel::Equality<RelView, ComplementView>::post(home, vx, cvy) != ES_OK)
    home.fail();
}

////////////////////////////////
// Intersection post function //
////////////////////////////////
void intersection(Space& home, RelVar x, RelVar y, RelVar z) {
  if (home.failed())
    return;

  if (!setCompatible(x.schema(), y.schema(), z.schema())) {
    throw Rel::SetIncompatible("intersection");
  }

  RelView vx(x), vy(y), vz(z);
  if (Rel::Intersection<RelView, RelView, RelView>::post(home, vx, vy, vz) !=
      ES_OK)
    home.fail();
}

/////////////////////////
// Union post function //
/////////////////////////
void Union(Space& home, RelVar x, RelVar y, RelVar z) {
  if (home.failed())
    return;

  if (!setCompatible(x.schema(), y.schema(), z.schema())) {
    throw Rel::SetIncompatible("union");
  }

  RelView vx(x), vy(y), vz(z);
  ComplementView cvx(vx), cvy(vy), cvz(vz);

  if (Rel::Intersection<ComplementView, ComplementView, ComplementView>::post(
          home, cvx, cvy, cvz) != ES_OK)
    home.fail();
}

///////////////////////
// Subset constraint //
///////////////////////
void subset(Space& home, RelVar x, RelVar y) {
  if (home.failed())
    return;

  if (!setCompatible(x.schema(), y.schema())) {
    throw Rel::SetIncompatible("subset");
  }

  RelView vx(x), vy(y);
  ComplementView cvy(vy);

  Relation empty =
      Relation::createEmpty(Rel::getRelationSpace(), x.glb().schema());

  // TODO: ConstRelView does not have dispose
  // ConstRelView emptyView(empty);
  RelVar em(home, empty, empty);
  RelView ev(em);

  GECODE_ES_FAIL((Rel::Intersection<RelView, ComplementView, RelView>::post(
      home, vx, cvy, em)));
}
/////////////////////////
// Disjoint constraint //
/////////////////////////
void disjoint(Space& home, RelVar x, RelVar y) {
  if (home.failed())
    return;

  if (!setCompatible(x.schema(), y.schema())) {
    throw Rel::SetIncompatible("disjoint");
  }

  RelView vx(x), vy(y);

  Relation empty =
      Relation::createEmpty(Rel::getRelationSpace(), x.glb().schema());
  /*
    The right way for posting this constraint is to create a constant view on an
    empty relation. This however produces a reference counting problem at the
    BDD level. A constant view has a relation as attribute but has no dispose
    method to call its destructor.

    TODO: for now I create a normal view and initialize it with equal lower and
    upper bounds. Ask in the mailing list how could I fix this.
   */
  // ConstRelView emptyView(empty);
  // GECODE_ES_FAIL((Rel::Intersection<RelView, RelView, ConstRelView>::post(
  //     home, vx, vy, emptyView)));
  //

  RelVar em(home, empty, empty);
  RelView ev(em);
  GECODE_ES_FAIL(
      (Rel::Intersection<RelView, RelView, RelView>::post(home, vx, vy, ev)));
}
////////////////////////////
// Cardinality constraint //
////////////////////////////
void cardinality(Space& home, RelVar x, IntVar y) {
  if (home.failed())
    return;

  RelView vx(x);
  IntView vy(y);

  GECODE_ES_FAIL((Rel::Cardinality<RelView, IntView>::post(home, vx, vy)));
}
}