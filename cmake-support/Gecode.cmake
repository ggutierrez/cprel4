set(DEFAULT_GECODE_INSTALL_DIR "${ep_base}/Install/gecode")
set(GECODE_INSTALL_DIR ${DEFAULT_GECODE_INSTALL_DIR} CACHE PATH
  "Path to Gecode installation")

set(GECODE_VERSION 4.2.1)
message(STATUS 
  "[Gecode] Version ${GECODE_VERSION} will be fetch and built")
message(STATUS
  "[Gecode] Install dir set to ${GECODE_INSTALL_DIR}"
)

set(GECODE_SOURCE_DIR "${ep_base}/Source/gecode")
set(GECODE_BUILD_DIR "${ep_base}/Build/gecode")

set(GECODE_CONFIGURE_OPTIONS "")
macro(GECODE_OPTION OPT)
  set(GECODE_CONFIGURE_OPTIONS "${GECODE_CONFIGURE_OPTIONS} ${OPT}")
endmacro()

## Variables used for Gecode configuration
GECODE_OPTION("--with-vis=${PROJECT_SOURCE_DIR}/rel.vis")
## Build Gecode with static and no share libraries 
GECODE_OPTION("--disable-shared --enable-static")
## Disable visibility
GECODE_OPTION("--disable-gcc-visibility")
## Set up Gecode modules to be built 
GECODE_OPTION("--disable-flatzinc")
GECODE_OPTION("--disable-minimodel")
GECODE_OPTION("--disable-driver")
GECODE_OPTION("--disable-examples")

message(STATUS "[Gecode] Disabling threads as BDD library does not support threaded search engines.")
GECODE_OPTION("--disable-thread")


## If Debug is enabled at the cmake level then enable it in the Gecode build.
if("${CMAKE_BUILD_TYPE}" MATCHES "Debug")
  GECODE_OPTION("--enable-debug")
  message(STATUS "[Gecode] Enabling debug mode!!")
endif()

## Build the gist tool only if qt is available.
#
#  It is up to Gecode to find Qt again, here we only detect Qt to know if Gecode
#  is going to build with gist support.
set(QT_MIN_VERSION 4.3)
find_package(Qt4 COMPONENTS QtCore QtGui)
include(${QT_USE_FILE})

include_directories(${QT_INCLUDES})
if(QT4_FOUND)
  message(STATUS "[Gecode] Building gist")
  GECODE_OPTION("--enable-qt --enable-gist")
else()
  GECODE_OPTION("--disable-qt --disable-gist")
endif()

# Create the configuration command
configure_file(configure-gecode.sh configure-gecode.sh @ONLY)

# TODO: use the same number of jobs to build gecode. Is it possible to get 
#       that information from cmake??
#
# TODO: On deployment be sure to fetch gecode from the website
ExternalProject_Add(
  gecode
  URL http://www.gecode.org/download/gecode-${GECODE_VERSION}.tar.gz
  SOURCE_DIR ${GECODE_SOURCE_DIR}
  BINARY_DIR ${GECODE_BUILD_DIR}
  INSTALL_DIR ${GECODE_INSTALL_DIR}
  UPDATE_COMMAND ""
  PATCH_COMMAND ""
  CONFIGURE_COMMAND bash ${PROJECT_BINARY_DIR}/configure-gecode.sh 
  BUILD_COMMAND make
  INSTALL_COMMAND make install    
)

# These are the results of a successful Gecode build. 
link_directories(${GECODE_INSTALL_DIR}/lib)
set(GECODE_LIBRARIES 
  gecodekernel
  gecodesupport
  gecodeint
  gecodeset
  gecodesearch 
)
if(QT4_FOUND)
  # On linux, the order in which the gecode libraries are linked is important.
  # for that reason, rather than doing an append we use set to put gecodegist 
  # at the beginning.
  #list(APPEND GECODE_LIBRARIES gecodegist)
  set(GECODE_LIBRARIES gecodegist ${GECODE_LIBRARIES} )
endif()

set(GECODE_INCLUDES ${GECODE_INSTALL_DIR}/include)

foreach(GECODE_LIB ${GECODE_LIBRARIES})
  add_library(${GECODE_LIB} STATIC IMPORTED)
  set_property(TARGET ${GECODE_LIB} PROPERTY 
               IMPORTED_LOCATION ${GECODE_INSTALL_DIR}/lib/lib${GECODE_LIB}.a)
  add_dependencies(${GECODE_LIB} gecode)
endforeach()
