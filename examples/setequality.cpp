/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "rel.hh"
#include <gecode/set.hh>
#include <gecode/search.hh>
#include <gecode/gist.hh>

using CuddAbstraction::Attribute;
using CuddAbstraction::makeDomain;

using Gecode::SetVar;
using Gecode::IntSet;

using namespace MPG;

class SetEqualityCSP : public Gecode::Space {
protected:
  RelVar x;
  SetVar y;

public:
  SetEqualityCSP(const Relation& xl0, const Relation& xg0)
      : x(*this, xl0, xg0)
      // , y(*this,IntSet::empty,0,xl0.domain())
  {
    // Get the unique column domain of xl0
    const Attribute& d = xl0.schema().asColumnDomains()[0];

    y = SetVar(*this, IntSet::empty, 0, d.domain()->max());

    equal(*this, x, y);
    // branch(*this,x,REL_VAL_MIN());
    branch(*this, y, Gecode::SET_VAL_MIN_INC());
  }

  void print(std::ostream& os) const {
    os << "X:" << x << std::endl;
    os << "Y:" << y << std::endl;
  }
  SetEqualityCSP(bool share, SetEqualityCSP& s) : Gecode::Space(share, s) {
    x.update(*this, share, s.x);
    y.update(*this, share, s.y);
  }
  virtual Space* copy(bool share) { return new SetEqualityCSP(share, *this); }
};

int main(int argc, char* argv[]) {

  auto& relHome = MPG::Rel::getRelationSpace();
  {
    Attribute c0(relHome, makeDomain(4));

    Schema cDom(relHome, {c0});

    SetEqualityCSP* g = new SetEqualityCSP(Relation::createEmpty(relHome, cDom),
                                           Relation::createFull(relHome, cDom));
#ifdef GECODE_HAS_GIST
    Gecode::Gist::Print<SetEqualityCSP> p("Solution");
    Gecode::Gist::Options o;
    o.inspect.click(&p);
    Gecode::Gist::dfs(g, o);
    delete g;
#else
    Gecode::DFS<SetEqualityCSP> e(g);
    delete g;
    unsigned int solutions = 0;
    while (Gecode::Space* s = e.next()) {
      solutions++;
      // static_cast<SetEqualityCSP*>(s)->print();
      delete s;
    }
    std::cout << "Solutions: " << solutions << std::endl;
#endif
  }
  std::cout << "References: " << relHome.zeroReferences() << std::endl;
  return 0;
}
