/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "rel.hh"
#include <gecode/search.hh>
#include <gecode/gist.hh>

using namespace MPG;

/**
 * @brief This CSP maintains an equality relation between relations on different
 *     schemas.
 *
 * The correspondence between the attributes is established by a domain mapping.
 */
class EqualityCSP : public Gecode::Space {
protected:
  RelVar x;
  RelVar y;

public:
  EqualityCSP(const Relation& xl0, const Relation& xg0, const Relation& yl0,
              const Relation& yg0, const AttributeMap& m)
      : x(*this, xl0, xg0)
      , y(*this, yl0, yg0) {

    rename(*this, x, m, y);
    branch(*this, x, REL_VAL_MIN());
    branch(*this, y, REL_VAL_MIN());
  }

  void print(std::ostream& os) const {
    os << "X:" << x << std::endl;
    os << "Y:" << y << std::endl;
  }
  EqualityCSP(bool share, EqualityCSP& s) : Gecode::Space(share, s) {
    x.update(*this, share, s.x);
    y.update(*this, share, s.y);
  }
  virtual Space* copy(bool share) { return new EqualityCSP(share, *this); }
};

int main(int argc, char* argv[]) {
  using CuddAbstraction::Attribute;
  using CuddAbstraction::AttributeDomain;
  using CuddAbstraction::makeDomain;

  auto& relHome = MPG::Rel::getRelationSpace();
  {
    AttributeDomain dom0 = makeDomain(2);
    AttributeDomain dom1 = makeDomain(2);
    AttributeDomain dom2 = makeDomain(2);

    Attribute c0(relHome, dom0), c1(relHome, dom1), c2(relHome, dom2),
        a0(relHome, dom0), a1(relHome, dom1), a2(relHome, dom2);

    Schema cDom(relHome, {c0, c1, c2}), aDom(relHome, {a0, a1, a2});

    AttributeMap cToA({{c0, a0}, {c1, a1}, {c2, a2}});

    EqualityCSP* g = new EqualityCSP(
        Relation::createEmpty(relHome, cDom), arithPlus(relHome, cDom),
        Relation::createEmpty(relHome, aDom), arithPlus(relHome, aDom), cToA);
#ifdef GECODE_HAS_GIST
    Gecode::Gist::Print<EqualityCSP> p("Solution");
    Gecode::Gist::Options o;
    o.inspect.click(&p);
    Gecode::Gist::dfs(g, o);
    delete g;
#else
    Gecode::DFS<EqualityCSP> e(g);
    delete g;
    unsigned int solutions = 0;
    while (Gecode::Space* s = e.next()) {
      solutions++;
      // static_cast<EqualityCSP*>(s)->print();
      delete s;
    }
    std::cout << "Solutions: " << solutions << std::endl;
#endif
  }
  std::cout << "References: " << relHome.zeroReferences() << std::endl;
  return 0;
}
