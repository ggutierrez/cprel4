/*
 *  Authors:
 *    Gustavo Gutierrez-Sabogal <gustavo.ggutierrez@gmail.com>
 *
 *  Copyright:
 *    Gustavo Gutierrez, 2013
 *
 *  Permission is hereby granted, free of charge, to any person obtaining
 *  a copy of this software, to deal in the software without restriction,
 *  including without limitation the rights to use, copy, modify, merge,
 *  publish, distribute, sublicense, and/or sell copies of the software,
 *  and to permit persons to whom the software is furnished to do so, subject
 *  to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 *  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 *  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 *  LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 *  OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 *  WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "rel.hh"
#include <gecode/search.hh>
#include <gecode/gist.hh>

using namespace MPG;

class TrivialCSP : public Gecode::Space {
protected:
  static const int n = 3;
  RelVarArray m;
  RelVar k;

public:
  TrivialCSP(const Relation& xl0, const Relation& xg0, const Relation& kl0,
             const Relation& ku0)
      : m(*this, n, xl0, xg0)
      , k(*this, kl0, ku0) {

    intersection(*this, m[0], m[1], m[2]);
    Union(*this, m[0], m[1], m[2]);

    projection(*this, m[0], kl0.schema(), k);
    branch(*this, m, REL_VAR_NONE(), REL_VAL_MIN());
  }

  void print(std::ostream& os) const {
    os << "\tm[" << m.size() << "] = " << m << std::endl;
    os << std::endl;
    os << "Proj: " << k << std::endl;
  }
  TrivialCSP(bool share, TrivialCSP& s) : Gecode::Space(share, s) {
    m.update(*this, share, s.m);
    k.update(*this, share, s.k);
  }
  virtual Space* copy(bool share) { return new TrivialCSP(share, *this); }
};

int main(int argc, char* argv[]) {
  using CuddAbstraction::Attribute;
  using CuddAbstraction::makeDomain;

  auto& relHome = MPG::Rel::getRelationSpace();
  {
    Attribute x(relHome, makeDomain(4)), y(relHome, makeDomain(4)),
        z(relHome, makeDomain(4));
    Schema sch_xyz = schema({x, y, z});
    Schema sch_xy = schema({x, y});

    Relation groundPlus = arithPlus(relHome, sch_xyz);
    Relation groundLeq = lessOrEqual(relHome, sch_xy);

    TrivialCSP* g =
        new TrivialCSP(empty(sch_xyz), groundPlus, empty(sch_xy), groundLeq);
#ifdef GECODE_HAS_GIST
    Gecode::Gist::Print<TrivialCSP> p("Solution");
    Gecode::Gist::Options o;
    o.inspect.click(&p);
    Gecode::Gist::dfs(g, o);
    delete g;
#else
    Gecode::DFS<TrivialCSP> e(g);
    delete g;
    unsigned int solutions = 0;
    while (Gecode::Space* s = e.next()) {
      solutions++;
      // static_cast<TrivialCSP*>(s)->print();
      delete s;
    }
    std::cout << "Solutions: " << solutions << std::endl;
#endif
  }
  std::cout << "References: " << relHome.zeroReferences() << std::endl;
  return 0;
}
